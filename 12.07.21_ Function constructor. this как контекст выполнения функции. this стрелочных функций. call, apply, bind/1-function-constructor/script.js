

// function createUser(firstName, lastName, sex, age) {  // функция которая создает юзеров
//   return {
//     name: {
//       first: firstName,
//       last: lastName,
//     },
//     sex,
//     age,
//     getFullName: function () {
//       return this.name.first + " " + this.name.last;
//     },
//     isAdult: function () {
//       if (this.age > 18) {
//         return true;
//       } else {
//         return false;
//       }
//     },
//   };
// }
// const v = createUser("firstName" , "lastName" , "f" , 30)
// console.log(v);
//


// const sayFn = function (){
//   console.log("hello");
// }
function User(firstName, lastName, sex, age) {  // это и есть фуенкцияконструктор и менуется с заглавной буквы!!
    // console.log("new.target" , new.target); //так проверить что делет нев таргет , в данный случай он будет давать ссылку на на функцию юзер
    if (!new.target) return new User((firstName , lastName , sex));
    this.firstName = firstName;
    this.lastName = lastName;
    this.sex = sex;
    this.age = age;
    // this.say = function (){
    //   console.log("hello");
    // }
    // this.say = sayFn

}
const User1 = new User("firstName" , "lastName" , "f" , 30) // важная фишка с new   //
const User2 = new User("firstName" , "lastName" , "f" , 17) // важная фишка с new

// v.say(); // что бы вызвать консоль та что в нутри функции

User.prototype.getFullName = function () {       // прототип связывает двох юзеров
    return this.firstName + ' ' + this.lastName;
}

User.prototype.isAdult = function () {  // проверять наш пользователь совершенно летний или нет
    if (this.age >= 18) {
        return true;
    } else {
        return false;
    }
}
console.log("User1: " , User1.getFullName(), User1.isAdult());
console.log("User2: " , User2.getFullName(), User2.isAdult());

// const user1 = new User("Chester", "Miller", "male", 34);
//
// const user2 = new User("Avery", "Hale", "female", 14);
//
// let obj = {
//   fn() {
//     console.log(this);
//   },
//   arrow: () => console.log(this),
// };

