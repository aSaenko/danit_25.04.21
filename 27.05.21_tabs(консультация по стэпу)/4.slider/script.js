

function changeSlide(direction) {
    const activeSlideBtnList = document.querySelectorAll(".toggles .slider-btn"); // перебирвем все кнопки
    const slideCount = activeSlideBtnList.length; // что бы не перескакивать на элемеенты которых нет
    const activeSlideBtn = document.querySelector(".toggles .slider-btn.active"); // вытягиваем кнопки с активом , именно активную кнопку
    const activeSlideIndex = Number(activeSlideBtn.dataset.index); // считывает какой элемент сейчас активный намберс изз за дата сет принимает только строки


    let updatedIndex;

    switch (direction) {
        case "prev":
            updatedIndex =
                activeSlideIndex - 1 <= 0 ? slideCount : activeSlideIndex - 1; // проверка по длине и интексе ,
            break;
        case "next":
            updatedIndex =
                activeSlideIndex + 1 > slideCount ? 1 : activeSlideIndex + 1;
            break;
    }

    activeSlideBtn.classList.remove("active");
    document
        .getElementById("js-slide-" + activeSlideIndex)
        .classList.remove("active");

    activeSlideBtnList[updatedIndex - 1].classList.add("active");
    document.getElementById("js-slide-"+ updatedIndex).classList.add("active");
}

document
    .querySelector(".btn-slide-arrow.prev")
    .addEventListener("click" , () => changeSlide("prev"));

document
    .querySelector(".btn-slide-arrow.next")
    .addEventListener("click" , () => changeSlide("next"));