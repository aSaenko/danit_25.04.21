
let response = [        // это и будет то что мы добавляем кнопкой!
    {
        title: "lerem 1",
        description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore\n" +
            "          eaque perspiciatis modi. Rerum officia reiciendis eveniet. Molestiae\n" +
            "          natus repudiandae debitis!",
        img: "https://www.cimton.com/images/media/demo.jpg",
        author: "Jhon doe",

    },
    {
        title: "lerem 2",
        description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore\n" +
            "          eaque perspiciatis modi. Rerum officia reiciendis eveniet. Molestiae\n" +
            "          natus repudiandae debitis!",
        img: "https://www.cimton.com/images/media/demo.jpg",
        author: "Jhon doe",

    },
    {
        title: "lerem 3",
        description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore\n" +
            "          eaque perspiciatis modi. Rerum officia reiciendis eveniet. Molestiae\n" +
            "          natus repudiandae debitis!",
        img: "https://www.cimton.com/images/media/demo.jpg",
        author: "Jhon doe",

    },
    {
        title: "lerem 4",
        description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore\n" +
            "          eaque perspiciatis modi. Rerum officia reiciendis eveniet. Molestiae\n" +
            "          natus repudiandae debitis!",
        img: "https://www.cimton.com/images/media/demo.jpg",
        author: "Jhon doe",

    },
];

let addMoreBtn = document.getElementById("js-add-more-btn");
addMoreBtn.addEventListener("click", function () {

    addMoreBtn.innerText = 'loading...';    // этим меняем текс кноки

    const container = document.getElementsByClassName("content");

    setTimeout(function () {  //  асинхронность
         // сюда надо вставить тупо все что в низу const porLIst
    },2000);

    const portList = response.splice(0,3);
    portList.forEach((post) => {
        const elPost = document.createElement('div');
        elPost.classList.add("post");
        elPost.innerHTML = `<img src="${post.img}" alt="" />
        <h2 class="${post.title}">Post 3</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore
          eaque perspiciatis modi. Rerum officia reiciendis eveniet. Molestiae
          natus repudiandae debitis!
        </p>
        <span class="author">${post.author}</span>`;

        container.append(elPost);
    });

    addMoreBtn.innerText = "Add more";

    if (response.length === 0){
        addMoreBtn.style.display = "none";
    }
});

