
let btnList = document.querySelectorAll(".toggle .tab-btn");

for (let i = 0; i <= btnList.length -1; i++){  // этим взяли все элементы
    const el = btnList[i];
        el.addEventListener("click", btnClickHandler);

}

function btnClickHandler(event) {  // то что будет вызыватся каждый раз при нажатии
    document.querySelector(".tab-btn.active").classList.remove("active");
    event.target.classList.add("active");
    document.querySelector(".tab-content.active").classList.remove("active");
    event.target.classList.add("active");

    const currentIndex = event.target.dataset.index;   // без этого не будет тянуть контент , говорит - при клике ищи событие на который нажат(таргет) , датасет ищет индекс
    document.getElementById("js-tab-"+ currentIndex).classList.add("active");

}
// почему то не работает , надо пересмотреть.