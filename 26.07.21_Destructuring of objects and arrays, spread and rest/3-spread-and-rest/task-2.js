// - об'єднати два об'єкта в один з назвою driver
// - з новоствореного об'єкту отримати вік та вивести значення в консоль
// - з новоствореного об'єкту отримати ім'я та прізвище, присвоївши їх у змінні firstName та lastName,
// та вивести значення в консоль
// - отримати з об'єкту driver номер посвідчення (id), яке за замовчуванням дорівнює 'sample' та
// вивести значення в консоль

const person = {
    name: {
        first: "Ryan",
        last: "Krin",
    },
    age: 24,
};

const license = {
    type: "B1",
    issuedDate: "12/4/2018",
    expirationDate: "12/4/2022",
    // id : 123,
};

let driver = {... person, ...license} ;
console.log(driver)

let {age , name:{first: firstName , last:lastName},id = "sample" } = driver;
console.log(age);
console.log(firstName);
console.log(lastName);
console.log(id);



