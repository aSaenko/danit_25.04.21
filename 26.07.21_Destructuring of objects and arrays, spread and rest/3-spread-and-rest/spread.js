const person = {
    name: "John",
    test: true,
};

const city = {
    city: "Kyiv",
    country: "Ukraine",
    test: false,
};

const user = {
    ...person,
    ...city,
};

const user2 = { ...user };

const obj = Object.assign({}, person, city);

console.log("user 1:", user);
user.test = "banana";
console.log("user 2:", user2);
console.log(user === user2);

const cities = ["Kyiv", "Lviv", "Odesa", "Kharkiv", "Dnipro"];

const [kyiv, lviv, ...other] = cities;

console.log(kyiv);
console.log(lviv);
console.log(other);


const fn = function (a,b,c , ...rest){  // три точки и рест означает что если буду больше обьектов их запишет в переменную рест,  это и есть спред
    console.log("fn" , a,b,c);
    console.log(arguments);  // для того что бы увидеть остальные обьекты которые написаны
}
fn(1,2,3,4,5,6) // остальные можно найти написав в консоле arguments


// пример с стрелочной фнкц
const fnArrow = ({name:id}, b, ...argsArrow) => {
    console.log("arrowFn:", name, id);
    console.log(argsArrow);
};

fnArrow({ name: "Test user", id: 987654321 }, 2, 3, true, "string", [], {});



