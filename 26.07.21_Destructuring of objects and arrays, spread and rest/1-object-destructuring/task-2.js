// - створити об'єкт вакансії для фронтенду (frontendVacancy) та бекенду (backendVacancy),
// які будуть мати наступний вигляд
// {
//    company: 'DAN.IT',
//    city: 'Kyiv',
//    skills: [...]
// }
// - вивести кожну вакансію в консоль

const geoLocation = {
    name: "Kyiv",
    type: "city",
    country: "Ukraine",
};

const company = {
    title: "DAN.IT",
    employeesCount: 32,
    type: "private",
};

const skills = {
    frontend: [
        {
            name: "JS",
            experience: 3,
        },
        {
            name: "React + Redux",
            experience: 2,
        },
        {
            name: "HTML5",
            experience: 2,
        },
        {
            name: "CSS3",
            experience: 2,
        },
    ],
    backend: [
        {
            name: "PHP",
            experience: 2,
        },
    ],
};

const {name: city} = geoLocation;
const {title:companyName} = company;
const {frontend,backend} = skills;

// const frontendVacancy = {company: companyName , city,skills: frontend};
// const backendVacancy = {company: companyName , city,skills: backend};

// console.log(frontendVacancy);
// console.log(backendVacancy);


//прмиер создания новых вакансий
let vacancyCreator = (company,city) => (skills) =>({company,city,skills});

const danItVacantionCreator = vacancyCreator(companyName,city);
const frontendVacancy = danItVacantionCreator(frontend);
const backendVacancy = danItVacantionCreator(backend);

console.log(frontendVacancy);
console.log(backendVacancy);

