const user = {
    firstName: "John",
    lastName: "Mikel",
    age: 21,
    isAdmin: true,
};

const person = {
    firstName: "Rachel",
    age: 23,
    isAdmin: true,
};


// const firstName = user["firstName"];  // так по обычному
// const userAge = user.age;

// пример деструктиризации
const {age , firstName , isAdmin , test} = person;
console.log(age)
console.log(firstName)
console.log(isAdmin)
console.log(test)

const {                       // можно переименовывать или переназначать
    age :personAge,
    firstName : personFirstName ,
    sAdmin: personIsAdmin ,
} = person;   // в этой строчке и заключается деструктуризация.

console.log("Person:" ,personAge )
console.log("Person:", personFirstName)
console.log("Person:", personIsAdmin)




// пример для чего переиминововать
const figure = {
    x_0_y_0: 12,
    x_0_y_1:4,
    x_1_y_0:28,
    x_1_y_1:10,
}

const {
    x_0_y_0: topLeft,
    x_0_y_1:topRight,
    x_1_y_0:bottomLeft,
    x_1_y_1:bottomRight,
} = figure

//пример сложного обьекта

const product ={
    name: "iPhone",
    model:"12ProMax",
    shipping:{
        free:false,
        price:1200,
        currency: "$",
    },
};

const {shipping:{free,price,currency}} = product;  //описываем ключи = с камим обьектом хотим работать

// console.log("Product[ship]:" , shipping);
console.log("Product[ship]:" , free);
console.log("Product[ship]:" , price);
console.log("Product[ship]:" , currency);


const productList = [product];
console.log(productList)






// // const firstName = user.firstName;
// // const lastName = user.lastName;
// // const age = user['age'];
// // const isAdmin = user.isAdmin;

// // get values
// // change naming
// const { firstName, lastName: surname, age: userAge, isAdmin: userIsAdmin } = user;

// // default values
// const { age: personAge, isAdmin: personIsAdmin = false, company = 'DAN.IT' } = person;

// console.log(firstName);
// // console.log(lastName);
// console.log('surname => ', surname);
// console.log('userAge => ', userAge);
// console.log('personAge => ', personAge);
// console.log('user is admin => ', userIsAdmin); // true
// console.log('person is admin => ', personIsAdmin); // true
// console.log('person company => ', company); // DAN.IT
// console.log(person);

// // rest operator
// // method destructuring
