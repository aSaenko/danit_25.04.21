// в масиві задано топ компаній по капіталізації по спаданню
//
// - присвоїти у змінну найбільшу компанію по капіталізації
// та вивести результат в консоль
// - присвоїти у змінну третю компанію по капіталізації
// та вивести результат в консоль
// - решту компаній додати в змінну others

const topCompanies = [
    "Apple",
    "Saudi Aramco",
    "Amazon",
    "Microsoft",
    "Alphabet",
];

// let a = topCompanies[0];
const [RichComp, , thirdComp , ... others] = topCompanies;

console.log(RichComp);
console.log(thirdComp);
console.log(others);