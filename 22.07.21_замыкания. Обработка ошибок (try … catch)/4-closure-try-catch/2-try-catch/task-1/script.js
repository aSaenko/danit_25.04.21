let jackPerson;
let jennyPerson;

function Person(name, age) {
  this.name = name;
  this.age = age;
}

Person.prototype.sayHi = function () {
  console.log(`Hello, I'm ${this.name}`);
};
