
let e = new Error("Опаньки О_о");

console.log("name" , e.name);
console.log("message" , e.message);
console.log("stack" , e.stack);

// console.log(e instanceof RangeError);
// throw new SyntaxError("Опаньки О_о 2");

// если данный из вне
const data = {
    test: true ,
    position: [],
    count: 0,
}


try { // используется для того что бы при парсинге данных код на ошибке не останавливася
    console.log("start fetch data");
    console.log("show spinner");
    // throw new Error("Worker not fount")
    console.log("data success");
}catch (error){
    console.log(error.message , error.stack);
    console.log("worker error");
}finally {   //тот код который должен выполнится в не зависимости есть ошибка или нет
    console.log("hide spinner");
}



// console.log("finish");

// const input = prompt('Enter you name and surname');
//
// try {
//
//     console.log(input);
//     const inputArr = input.split(' ');
//
//     const [firstName, lastName] = inputArr;
//
//     console.log({ firstName, lastName });
//
// } catch (err) {
//     console.error(err);
//     alert('Enter valid data');
// } finally {
//     console.log('in any case');
// }
//
// console.log('another script');