// let position = 'backend';
// // const firstName = 'Jack';
//
//
// // function sayHiBackendDev(name) {
// //     let position = 'backend';
// //     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// // }
//
// // function sayHiFrontendDev(name) {
// //     let position = 'frontend';
// //     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// // }
//
// // function sayHiMarketing(name) {
// //     let position = 'marketing';
// //     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// // }
//
//
// function sayHi(position) {
//     return function (name) {
//         console.log(`Hi, I'm ${name}, I work as ${position} developer`);
//     }
// }
//
// const sayHiSEO = sayHi('SEO');
// const sayHiSMM = sayHi('SMM')
// const sayHiBackend = sayHi('Backend');
//
// // console.log(sayHi);
// // console.log(sayHiSEO);
//
// sayHiSEO('Marry');
// sayHiSEO('Mike');
//
// sayHiBackend('John');
// sayHiBackend('Jack');

// sayHi

// sayHiFrontendDev('Jack');

// position = 'frontend';
// sayHiFrontendDev('Jack');

// sayHiBackendDev('John');

/**
 * Counters счетчик
 */

function createCounter() {      //пример функции замыкания
    let count = 0;

    function increment() {
        return ++count;
    }

    return increment;   // из за этого ретерна и создается это замыкание
}

const counter1 = createCounter();

console.log("counter value: ", counter1()); // каждый вызов будет добалять единицу
console.log("counter value: ", counter1());
console.log("counter value: ", counter1());

const counter2 = createCounter();

console.log("counter value: ", counter2());
console.log("counter value: ", counter2());
console.log("counter value: ", counter2());

const counter3 = counter1;
console.log("counter value: ", counter3());  // будет 4 так как он опирается на ссылку крайнего счета


// 2й пример
const createCounterFn = () => {

    let count = 0;
    return () => ++count;
};

const plus = function (a) {
    return function (b) {
        return a + b;
    };
};

// const plusFn = (a) => (b) => a + b ;  // та же функция стрелочной функцией

const plus2 = plus(2);
const plus5 = plus(5);

console.log(plus5(7));
console.log(plus2(6));

// пример замыкания с 2мя функциями в нутри

function urlGenerator(domain) {
    return function (url) {
        return `https://${url}.${domain}`;
    }
}

const comUrl = urlGenerator("com");

console.log(comUrl("google"))
console.log(comUrl("Netflix"))


//пример замыкания с обьектами и строками

function bind(context , fn){
    return function (...args){
        fn.apply(context, args);  //вызывает функцию с указанным значением this и аргументами, предоставленными в виде массива
    }
}
function logPerson() {
    console.log(`Person: ${this.name}, ${this.age},${this.job}`)
}

const person1 = {name: "Михаил" , age : 22 , job: "Frontend"}
const person2 = {name: "Елена" , age : 19 , job: "SMM"}

bind(person1,logPerson)()  // хз чего так вызываетс, но только так работает
bind(person2,logPerson)()