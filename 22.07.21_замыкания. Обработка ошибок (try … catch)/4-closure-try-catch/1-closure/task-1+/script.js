/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */

// let ukrCount = 0 ;
// let gerCount = 0 ;
// let polCount = 0 ;
//
// function incrementCountry(countryName, counter){
//     const dif = 1000000
//
//     switch (countryName){
//         case "Ukraine":
//             ukrCount += dif;
//             console.log(
//                 `Населення ${countryName} збільшилося на ${dif} і становить ${counter}`
//             );
//             break
//     }
//     counter = counter + dif;
//     console.log(`Населення ${countryName} збільшилося на ${dif} і становить ${counter}`)
//     return counter;
// }
// incrementCountry("Ukraine", ukrCount);
// incrementCountry("Ukraine", ukrCount);
//
// incrementCountry("Germany", gerCount);
// incrementCountry("Germany", gerCount);
// incrementCountry("Germany", gerCount);
// incrementCountry("Germany", gerCount);
//
// incrementCountry("Poland", polCount);



    // решение с помощью классов
// class Country{
//     dif = 1000000
//     count = 0;
//
//     constructor(countryName, ) {
//         this.countryName = countryName;
//     }
//
//     increment(){  //увеличивает
//         this.count += this.dif;
//         console.log(`Населення ${this.countryName} збільшилося на ${this.dif} і становить ${this.count}`)
//     }
// }
//
// const Ukraine = new Country("Ukraine");
// const Poland = new Country("Poland");
// const Germany = new Country("Germany");
//
// Ukraine.increment()
// Ukraine.increment()
// Ukraine.increment()
//
// Poland.increment()
//
// Germany.increment()
// Germany.increment()
// Germany.increment()
// Germany.increment()

// c замыканием
function createCountryCounter(countryName) {
    let count = 0;
    const dif = 1000000

    function increment (){
        count += dif;
        console.log(`Населення ${countryName} збільшилося на ${dif} і становить ${count}`)
    }
    return increment;
}
const ukraineIncrement = createCountryCounter("Ukraine");
const polandIncrement = createCountryCounter("Poland");
const germanyIncrement = createCountryCounter("Germany");

ukraineIncrement();
ukraineIncrement();

polandIncrement();

germanyIncrement();