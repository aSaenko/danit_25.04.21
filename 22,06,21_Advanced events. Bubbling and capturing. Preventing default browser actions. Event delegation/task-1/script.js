/**
 * При натисканні на кнопку Закрити ховати модальне
 * вікно. При цьому alert не повинен спрацьовувати
 */

// Dont' touch this code
const modalEl = document.querySelector(".modal");
modalEl.addEventListener("click", () => alert("modal clicked"));

// Write your own code below

const elCloseBtn = document.querySelector(".close-btn");
elCloseBtn.addEventListener("click", (e) => {
  e.stopPropagation();
  modalEl.style.display = "none";
});
