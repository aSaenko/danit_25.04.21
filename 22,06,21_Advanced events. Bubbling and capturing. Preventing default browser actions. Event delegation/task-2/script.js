/**
 * Давати змогу помітити Subscribe to news
 * тільки якщо прийняті умови користування
 * (помічений Accept policy чекбокс)
 *
 */

function subscribeHandler(event) {
  const policyCheckbox = document.getElementById("policy-checkbox");
  console.log(policyCheckbox.checked);
  if (!policyCheckbox.checked) {
    event.preventDefault();
  }
}

const elSubscribe = document.getElementById("news-checkbox");

elSubscribe.addEventListener("click", subscribeHandler);
