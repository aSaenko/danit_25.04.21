/**
 * При натисканні на посилання не перенаправляти
 * користувача на це посилання, а просто відображати
 * його в блоці нижче
 *
 * Наприклад, при натисканні на mdn в параграфі
 * потрібно вивести It's link to https://developer.mozilla.org
 */

const clickHandler = (event) => {
  event.preventDefault();

  const elP = document.getElementById("link-value");
  elP.innerText = event.target.href;
};

const listLink = document.getElementsByTagName("a");
for (i = 0; i <= listLink.length - 1; i++) {
  listLink[i].addEventListener("click", clickHandler);
}
