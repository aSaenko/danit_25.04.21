// const a = document.querySelector('#google-link');
// const checkbox = document.querySelector('#checkbox');
// const listEl = document.querySelector('.list');
// checkbox.addEventListener('click', function (event) {
//     event.preventDefault();
// })

// const liItems = document.querySelectorAll('li');
// // console.log(li);

// // for (const li of liItems) {
// //     li.addEventListener('click', function (event) {
// //         if (!this.classList.contains('disabled')) {
// //             this.style.textDecoration = 'line-through';
// //         }
// //     })
// // }

// console.log(a);
// a.addEventListener('click', function (event) {
//     event.preventDefault();
//     // alert('clicked');

//     a.insertAdjacentHTML('afterend', '<p>Link clicked</p>');
// })

// listEl.addEventListener('click', function (event) {
//     console.log(this);
//     console.log(event.target);
//     if (!event.target.classList.contains('disabled')) {
//         event.target.style.textDecoration = 'line-through';
//     }
// })

// const li = document.createElement('li');
// li.innerText = 'Sixth';

// listEl.insertAdjacentElement('beforeend', li);

const elDiv = document.getElementById("div");
const elP = document.getElementById("p");
const elSpan = document.getElementById("span");

const checkbox = document.getElementById("checkbox");

checkbox.addEventListener("click", (e) => {
  e.preventDefault();
  console.log("STOP!");
});

// elDiv.addEventListener("click", (e) => {
//   alert("Click DIV");
//   console.log("DIV", "\n", this, e.currentTarget);
// });
// elP.addEventListener("click", (e) => {
//   e.stopPropagation();
//   alert("Click P");
//   console.log("P", this, "\n", e.currentTarget);
// });
// elSpan.addEventListener("click", (e) => {
//   alert("Click SPAN");
//   console.log("SPAN", this, "\n", e.currentTarget);
// });
