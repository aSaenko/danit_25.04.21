/**
 * При натисканні на кнопку Save заповнювати
 * таблицю нижче даними з форми
 *
 */

const SAVE_FIELDS = ["first-name", "last-name", "age"];
// const saveFirstName = inputSaveCreator("first-name");
// const saveLastName = inputSaveCreator("last-name");
// const saveAge = inputSaveCreator("age");

const submitHandler = (event) => {
  event.preventDefault();
  SAVE_FIELDS.forEach((key) => {
    const saveAction = inputSaveCreator(key);
    saveAction();
  });

  //   saveFirstName();
  //   saveLastName();
  //   saveAge();
};
const elSaveBtn = document.querySelector("#user-form [type='submit']");
elSaveBtn.addEventListener("click", submitHandler);

const getInput = (key) => {
  let inputElement = document.getElementById("input-" + key);
  return inputElement.value;
};

const setInput = (key, value) => {
  let tableCell = document.querySelector(`.${key}-value`);
  tableCell.innerText = value;
};

const resetInput = (key) => {
  let inputElement = document.getElementById("input-" + key);
  inputElement.value = "";
};

function inputSaveCreator(key) {
  return function () {
    const value = getInput(key);
    setInput(key, value);
    resetInput(key);
  };
}
