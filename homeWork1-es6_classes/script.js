

class Employee {
    constructor(name,age ,salary) {
        this._name = name;
        this.age = age;
        this.salary = salary;
    }

    getName(){
        return `${this._name}`
    }

    setName(){
        this._name = this._name + "125";
    }

}

class Programmer extends Employee{
        constructor(name,age ,salary , lang ) {
            super(name,age ,salary);
            this.lang = lang;
        }
        getSalary(){
            return this.salary * 3
        }
}


const employee = new Employee("Andrey","26" , "25000" );
const programmer = new Programmer("Andrey","26" , "25000" ,"english");
const programmer2 = new Programmer("Nik","57" , "5475" ,"italy");
const programmer3 = new Programmer("Sam","47" , "3644" ,"UK");

employee.setName();
console.log(employee.getName(),programmer );
console.log(programmer , programmer2 , programmer3);
console.log(programmer.getSalary());
console.log(programmer2.getSalary());

