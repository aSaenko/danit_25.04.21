const searchButton = document.querySelector("button[type=submit]");
const userContainer = document.querySelector(".user");
console.log(searchButton);

searchButton.addEventListener("click", (event) => {
  event.preventDefault();
  let userId = document.querySelector("input[type=text]").value;
  document.querySelector("input[type=text]").value = "";

  getUser(userId)
    .then(({ name, website, email }) => {
      userContainer.textContent = "";
      userContainer.insertAdjacentHTML(
        "afterbegin",
        `<h2>${name}</h2> <a href="${website}">${website}</a> <a href="mailto:${email}">${email}</a> `
      );
    })
    .catch((err) => console.error(err));
});

function get(url, queryParams) {
  const queryParamsString = new URLSearchParams(queryParams);

  return fetch(`${url} ${!!queryParams ? "?" + queryParamsString : ""}`).then(
    (resp) => {
      if (resp.ok) {
        return resp.json();
      } else {
        return new Error("fetch oops... i did it again!!!");
      }
    }
  );
}

function getUser(id) {
  const baseUrl = "https://ajax.test-danit.com/api/json";
  return get(baseUrl + "/users/" + id);
}
