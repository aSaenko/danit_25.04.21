// const baseUrl = "https://ajax.test-danit.com/api/json";
//
// function get(url, queryParams) {
//     const queryParamsString = new URLSearchParams(queryParams);
//
//     return fetch(`${url} ${!!queryParams ? "?" + queryParamsString : ""}`).then(
//         (resp) => {
//             if (resp.ok) {
//                 return resp.json();
//             } else {
//                 return new Error("fetch oops... i did it again!!!");
//             }
//         }
//     );
// }
//
// function patch(body , queryParams){
//
//     return fetch(`${url}`,{
//         method:"PATCH",
//         body: JSON.stringify(body)}).then(
//         (resp) => {
//             if (resp.ok) {
//                 return resp.json();
//             } else {
//                 return new Error("fetch oops... i did it again!!!");
//             }
//         });
// }
//
//
// function getPostById(id){
//
//     return get(`${baseUrl}/posts/${id}`);
// }
//
// function updatePost(id , body){
//     return patch(`${baseUrl}/posts/${id}`, body);
// }
//
// const postListElement = document.getElementById("post");
//
// postListElement.addEventListener("change" ,(event)=>{
//     const postId = postListElement.value;
//     // console.log(postId);
//
//
//     getPostById(postId)
//         .then((title,body)=>{
//             document.getElementById("title").value = title;  // этим добавили контент в строки при выборе поста
//             document.getElementById("body").value = body;
//         })
//         .catch((err)=> console.log(err));
// } )
//
// const btnUpdate = document.querySelector("button[type = submit]")
//
// btnUpdate.addEventListener("click" , (event)=>{
//     event.preventDefault();
//
//     const postId = postListElement.value;
//     const updatePost = {
//         title: document.getElementById("title").value,
//         body: document.getElementById("body").value,
//     };
//
//     updatePost(postId,updatePost)
//         .then(() => alert("success"))
//         .catch((err) => console.log(err))
// })
// //advanced
// function getPosts(){
//     return get(`${baseUrl}/posts`);
// }
//
// document.addEventListener("DOMContentLoader", ()=>{
//     getPosts()
//         .then((posts)=>{
//             postListElement.innerHTML =
//                 `<option value="none" disabled selected>ID поста</option>`;
//
//             posts.forEach(({id})=>{
//                 postListElement.insertAdjacentHTML(
//                     'beforeend',
//                     `<option value="${id}">${id}</option>`);
//             })
//         })
// })

// СКОПИРОВАНО!
const baseUrl = "https://ajax.test-danit.com/api/json";

function get(url, queryParams) {
    const queryParamsString = new URLSearchParams(queryParams);

    return fetch(`${url} ${!!queryParams ? "?" + queryParamsString : ""}`).then(
        (resp) => {
            if (resp.ok) {
                return resp.json();
            } else {
                return new Error("fetch oops... i did it again!!!");
            }
        }
    );
}

function patch(url, body) {
    return fetch(`${url}`, {
        method: "PATCH",
        body: JSON.stringify(body),
    }).then((resp) => {
        if (resp.ok) {
            return resp.json();
        } else {
            return new Error("fetch oops... i did it again!!!");
        }
    });
}

function getPostById(id) {
    return get(`${baseUrl}/posts/${id}`);
}

function updatePost(id, body) {
    return patch(`${baseUrl}/posts/${id}`, body);
}

const postListElement = document.getElementById("post");

postListElement.addEventListener("change", (event) => {
    const postId = postListElement.value;

    getPostById(postId)
        .then(({ title, body }) => {
            document.getElementById("title").value = title;
            document.getElementById("body").value = body;
        })
        .catch((err) => console.error(err));
});

const btnUpdate = document.querySelector("button[type=submit]");

btnUpdate.addEventListener("click", (event) => {
    event.preventDefault();

    const postId = postListElement.value;
    const udatedPost = {
        title: document.getElementById("title").value,
        body: document.getElementById("body").value,
    };

    updatePost(postId, udatedPost)
        .then(() => alert("success"))
        .catch((err) => console.error(err));
});

function getPosts() {
    return get(`${baseUrl}/posts`);
}

document.addEventListener("DOMContentLoaded", () => {
    getPosts().then((posts) => {
        postListElement.innerHTML =
            '<option value="none" disabled selected>ID поста</option>';

        posts.forEach(({ id }) => {
            postListElement.insertAdjacentHTML(
                "beforeend",
                `<option value="${id}">${id}</option>`
            );
        });
    });
});
