// Додати функционал пошуку инф про користувач за ID , використов
// наявне API

// робити запит для отримання инф тильки по отдному користувачу
// видображати имя у вигляди текст




const searchButton = document.querySelector("button[typt = submit]")
const userContainer = document.querySelector(".user");
console.log(searchButton);

searchButton.addEventListener("click" , (event)=>{
    event.preventDefault()
    let userId = document.querySelector("input[type=text]").value = ""; // этим зделано что бы постоянно с новой строки
    // console.log(userId);
    getUser(userId)
        .then(({name , website , email}) =>{
            userContainer.textContent = ""  // этим зделано что бы постоянно с новой строки
            userContainer.insertAdjacentHTML("afterbegin",
                `<h2>${name}</h2> <a href = "${website}">${website}</a> <a href="mailto:${email}"></a>`
            );
        })
        .catch((err)=> console.log(err))
});

function get (url , queryParams){
    const queryParamsString = new URLSearchParams(queryParams)

    return fetch(`${url} ${!!queryParams? '?' +queryParamsString :""}`)
        .then((resp)=>{
            if (resp.ok){
                return resp.json()
            } else {
                return new Error("fetch oops i did it again")
            }
        })
}

function getUser(id){
    const baseUrl = "https://ajax.test-danit.com/api/json"
    return get(baseUrl + '/users/' + id);
}