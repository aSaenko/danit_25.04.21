const gulp = require("gulp");
const concat = require("gulp-concat");
const clean = require("gulp-clean");

gulp.task("buildStyle" , () => {
    return gulp
        .src("src.css/main.css") // тут возьми  если *.css - означает беру все файлы с css
        .pipe(concat("style.scss")) // соединяет все стили
        .pipe(gulp.dest("build")) // сложи в дерикторию
});

gulp.task("clean" , () => {
    return gulp.src("build" , { read: false }).pipe(clean());
});

gulp.task("buildHtml" , () => {
    return gulp
    .srs("src/*.html" ,{allowEmpty : true})
        .pipe(minHtml({ collapseWhitespace : true}))
        .pipe(gulp.dest("build"));
});

// watch
gulp.task("watchCss" , () => {
    return gulp.watch("src/css/**/*.css" ,gulp.series("clean" , "buildStyle"))
});

gulp.task("watchHtml" , () => {
    return gulp.watch("src/*.html" ,gulp.series("clean" , "buildHtml"))
});

// paraleльный ,  сначала удаляет потом добавляет
gulp.task("build" , gulp.series([
    "clean" ,
    gulp.parallel([
        "buildCSS" ,
        "copyHTML"
    ])
]));


//gulp uglify
