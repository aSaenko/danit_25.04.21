# Адаптувати сторінку
Адаптувати сторінку використовуючи наступні брейкпойнти:
* tablet (768px)
* laptop (1024px)
* large-laptop (1280px)

## Mobile
![](mobile.png)

## Tablet
![](tablet.png)

## Laptop
![](laptop.png)

## Large Laptop
![](large-laptop.png)
