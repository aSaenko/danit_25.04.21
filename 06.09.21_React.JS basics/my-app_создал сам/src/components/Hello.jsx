import React,{ Component } from "react";
//вариант без классов
// function Hello(props){
//         const {
//             user,
//             showLastName
//         } = props

//         return(
//         <div>
//           <h1>Hi {user.firstName} {showLastName && user.lastName}!</h1>
//         </div>
//         )
// }


// тот же вариант стрелочной функц
// const Hello = ({user, showLastName}) => (<div><h1>Hi {user.firstName} {showLastName && user.lastName}!</h1></div>);



// вариант класом
class Hello extends Component {
    render() {
        console.log('props:', this.props);
        const {
            user,
            showLastName
        } = this.props

        return (
            <div>
                <h1>Hi {user.firstName} {showLastName && user.lastName}!</h1>
            </div>
        );
    }
}

export default Hello;