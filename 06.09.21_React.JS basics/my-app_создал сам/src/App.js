import React from "react";

//
// const App = () => (
//     <div>
//
//     </div>
// );

//создаем счетчик по которому будем менять температут +-

class App extends React.Component {
    constructor(props) {
        super(props);
        // описываем состояние
        this.state = {temparture: 18};

        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    render() {
        const currentStyle = {
            color:
                this.state.temparture < 11
                    ? 'blue'
                    : this.state.temparture > 24
                        ? "red" :
                        "green",
        }

        return (
            <div>
                <button onClick={this.decrement}>-</button>
                Current temparture: {this.state.temparture}
                <span style={currentStyle}>{this.state.temparture}</span>
                {this.getStatus()}
                <button onClick={this.increment}>+</button>
            </div>
        )
    }

    //увеличивает
    increment() {
        if (this.state.temparture < 35) {
            this.setState({temparture: this.state.temparture + 1});
        }
    }

    // уменьшает
    decrement() {
        if (this.state.temparture <= 0) return;
        this.setState({temparture: this.state.temparture - 1})
    }

    getStatus() {
        if (this.state.temparture < 11) return " Cold";
        if (this.state.temparture > 20) return " Hot";

    }
}
export default App;