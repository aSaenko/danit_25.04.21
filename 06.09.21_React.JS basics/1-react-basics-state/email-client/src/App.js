import React from 'react'
import './App.scss';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      title: 'Hello world from constr',
      counter: 0
    }
  }

  updateTitle = (event) => {
    console.log('clicked update title');
    this.setState({ title: 'hello' })
  }

  increment = event => {
    console.log('clicked counter');
    this.setState({ counter: this.state.counter + 1})
  }

  render() {
    const { title, counter } = this.state

    return (
      <div className="container">
        <h1 className="title">{title}</h1>
        <p style={ { color: 'red' } }>lorem its me</p>
        <button onClick={this.updateTitle}>Change</button>
        <p>Counter: {counter}</p>
        <button onClick={this.increment}>Increment</button>
      </div>
    )
  }
}

export default App;


/*   render() {
    return React.createElement('div', { className: 'container' }, [
      React.createElement('h1', { className: 'title'}, 'Hello world'),
      React.createElement('p', { style: { color: 'red' } }, 'lorem its me')
    ])
  } */