import types from './types'

// const initialState = {
//     // counter: 0,
//     // emails: [],
//     // isEmailsLoaiding: false,
//     // sync: false,
// }

const initialState = JSON.parse(localStorage.getItem("redux")) || {};


function reducer (state = initialState,action){
    switch(action.type) {
        case types.SET_COUNTER:
            return {
                ...state,
                counter: action.payload
            }
        case types.PLUSE:
            return {
                ...state, counter: state.counter+1
            }

            case types.MINUSE:
                return {
                    ...state, counter: state.counter-1
                }
        case types.SET_EMAILS:
                return {
                    ...state,
                    emails: action.payload
                }
        case types.SET_EMEAIL_LOADING_STATUS:
            return {
                ...state, isEmailsLoaiding:action.payload
            }
        case types.FETCH_PROCESING:
            return {
                ...state, isEmailsLoaiding:true
            }
        case types.FETCH_REGECTING:
            return {
                ...state, isEmailsLoaiding:false, emails:[]
            }
        case types.FETCH_SUCCESS:
            return {
                ...state, isEmailsLoaiding:false, emails:action.payload
            }
        case types.SET_SYNC:
            return {
                ...state, sync:action.payload
            } 
            
        case types.SET_FAVORITE:
            return {
                ...state, emails: state.emails.map((email) => email.id == action.payload ? {...email, isFavorite: !email.isFavorite} : email)
            }

        case types.SET_AUTSTATUS:
            return {
                ...state, isAuth: action.payload, 
            }

                default:
                    return {
                    ...state
                }
    }
}


// reducer({counter: 2}, { type: 'SET_COUNTER', payload: 11})

export default reducer;