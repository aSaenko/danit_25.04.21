import store from "./store";
import types from "./types";
import getEmails from '../api/emails';

export const pluseCounter = () => ({
    type: types.PLUSE
})

export const minuseCounter = () => ({ type: types.MINUSE})
export const setCounter = (value) => ({ type: types.SET_COUNTER, payload: value })
export const setEmails = (value) => ({ type: types.SET_EMAILS, payload: value })
export const fetchProcesing = () => ({ type: types.FETCH_PROCESING,})
export const fetchRegecting = () => ({ type: types.FETCH_REGECTING,})
export const fetchSuccess = (value) => ({ type: types.FETCH_SUCCESS, payload: value })
export const setSync = (value) => ({ type: types.SET_SYNC, payload: value})
export const setFavorite = (id) => ({type: types.SET_FAVORITE, payload: id})
export const setAuth = (value) => ({type: types.SET_AUTSTATUS, payload: value})

export const fetchEmails = () => dispatch => {
    dispatch({ type: types.FETCH_PROCESING}) //...state, isEmailsLoaiding:true
    dispatch(setSync(false))

    getEmails()
    .then(response => response.json())
    .then(emails => {
        dispatch(fetchSuccess(emails)) //...state, isEmailsLoaiding:false, emails: emailsFromResponse
    })
    .catch(error => {
        dispatch(fetchRegecting()) //...state, isEmailsLoaiding:false, emails:[]
    })
    .finally(() => {
        setTimeout(() => dispatch(setSync(true)), 5000)
    })
}






// const linkToPath = (path) => {
//     if(isBrowsrRouter){
//         return ({
//             type: 'CHANGE_PATH_BROWSER_ROUTER',
//             payload: path
//         })
//     }else{
//         return ({
//             type: 'CHANGE_PATH_HASH_ROUTER',
//             payload: path
//         })
//     }
// }


// disapth(linkToPath('/roduct/list/123'));
// const mapStateToProps = state => ({
//     a: state.a,
//     b: state.b,
// })

// const mapDispatchToProps = dispatch => ({
//     setAValueAction: (value) => dispatch({type: 'dispatch_a', payload: value}),
//     setBValueActtion: (value) => dispatch({type: 'dispatch_b', payload: value}),
// })

// const connect = (mapStateToProps, maptDispathToProps) => Component => props => {
//     const storeProps = mapStateToProps(store.getStore());
//     const storeDispatchrops = maptDispathToProps(store.dispatch);
    

//     return <Component {...props} {...storeProps} {...storeDispatchrops} />
// }

// const HelloWithStore = connect(mapStateToProps, mapDispatchToPro)(<Hello />)

// render (<HelloWithStore />)

// dispatch = useDispatch();

// const setAValueAction = value => dispatch({...})
// const a = useSelector( state => state.a)