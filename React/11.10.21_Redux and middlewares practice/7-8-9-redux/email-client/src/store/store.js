import {createStore,  applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'


//   function applyMiddleware(middleware) {
//     return function createStoreWithMiddleware(createStore) {
//       return (reducer, state) => {
//         const store = createStore(reducer, state)
//         return {
//           dispatch: action => {
//               const middlewareWithStoreObj = middleware(store)
//               const middlewareWithStoreObjWithDispatch = middlewareWithStoreObj(store.dispatch)
//               middlewareWithStoreObjWithDispatch(action)
//           },
//           getState: store.getState,
//         }
//       }
//     }
//   }

// MIDLEWARE
function logger({ getState }) {
    return next => action => {
      console.log('state before dispatch', getState());
      console.log('will dispatch', action)
  
      // Call the next dispatch method in the middleware chain.
      const returnValue = next(action)
  
      console.log('state after dispatch', getState())
  
      // This will likely be the action itself, unless
      // a middleware further in chain changed it.
      return returnValue
    }
  }

  function syncWithLocalStorage({ getState }) {
    return next => action => {
     
      const updatedAction = next(action)
      localStorage.setItem('redux', JSON.stringify(getState()))
      return updatedAction
    }
  }

  

// MIDLEWARE

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f => f)

const store = createStore(
    reducer,
    compose(applyMiddleware(thunk, syncWithLocalStorage), devTools),
)

console.log('redux store', store);

export default store