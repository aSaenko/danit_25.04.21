import React, { useState, useEffect } from "react";
import {useSelector, useDispatch} from "react-redux"
import authApi from "../api/auth";
import {pluseCounter, minuseCounter, setCounter,fetchEmails} from "../store/actions"


const ViewA = () => {
  const emailsList = useSelector(state => state.emails);
  const isLoading = useSelector(state => state.isEmailsLoaiding)
  const dispatch = useDispatch()
  
  return (<div>
    <h2> Email test: {JSON.stringify(isLoading)}</h2>
    <h2> Email list: {JSON.stringify(emailsList)}</h2>
    <button onClick={() => dispatch(fetchEmails())}>Fetch emails</button>
  </div>)
}


const HomePage = () => {
  const initalCounter = useSelector(state => state.counter);
  const isSync = useSelector(state => state.sync);
  // const [counter, setCounter] = useState(initalCounter);

  const dispatch = useDispatch()

  return <div>
    <h1>Hello {authApi.getAuthStatus() ? "friend" : "guest"}</h1>
    counter:{initalCounter}
    <button onClick={() => dispatch(pluseCounter())}> + </button>
    <button onClick={() => dispatch(minuseCounter())}> - </button>
    <button onClick={() => dispatch(setCounter(11))}>set counter value 11</button>

    
    {isSync && <button onClick={() => dispatch(fetchEmails())}>Sync</button>} 
    <hr/>
    <ViewA />
    </div>;
};

// dispatch({ type: 'SET_COUNTER', payload: 11})

export default HomePage;
