import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import authApi from "../api/auth";
import PleaseLoginPage from '../pages/PleaseLoginPage';
import {useSelector} from 'react-redux'


export default ({props, children}) => {
    const auth = useSelector((state) =>{
        return state.isAuth;
    })
    return (<Route {...props} >{auth ? children : <Redirect to="/login" /> }</Route>);
}