import React, {useState} from 'react';
import authApi from "../api/auth";
import {useSelector, useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {setAuth} from '../store/actions'

const LoginForm = (props) => {
    const dispatch = useDispatch();
    
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const authStatus = useSelector((state)=>{
        return state.isAuth;
    })
    if (authStatus) return <Redirect to='/' />
    return (

        <form>
            <label for="userName">Name</label>
            <input type="text" name="userName" id="userName" value={userName} onChange={e =>{
                setUserName(e.target.value);
            }} />
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value={password} onChange={e =>{
                setPassword(e.target.value);
            }}/>
            <button type="submit" onClick={e => {
                e.preventDefault();

                authApi.signIn(userName, password);

                if(!authApi.getAuthStatus()) {alert('Please enter your username and password')}
                else{
                   dispatch(setAuth(authApi.getAuthStatus()));
                };
            }}>Submit</button>
        </form>
    )
}

export default LoginForm;