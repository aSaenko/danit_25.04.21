import React, { useState, useEffect } from "react";
import MailsList from "../components/MailsList/MailsList";
import { getEmails } from "../api/emails";
const ArchivePage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [emails, setEmails] = useState([]);

  useEffect(() => {
    getEmails()
      .then((emails) => {
        setEmails(emails);
        localStorage.setItem("emails", JSON.stringify(emails));
      })
      .finally(() => setIsLoading(false));
  }, []);
  const readedEmails = emails.filter(({ isRead }) => !!isRead);
  return (
    <div>
      {isLoading ? (
        <p>Loading..</p>
      ) : (
        <>
          <MailsList title="Archive" mails={readedEmails} />
        </>
      )}
    </div>
  );
};

export default ArchivePage;
