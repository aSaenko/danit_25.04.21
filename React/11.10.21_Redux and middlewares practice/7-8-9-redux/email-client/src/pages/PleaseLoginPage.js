import React from 'react';

export default () => (<div>
    <h1>Dear user, please login</h1>
    <p>Go to /login page and try again.</p>
</div>);