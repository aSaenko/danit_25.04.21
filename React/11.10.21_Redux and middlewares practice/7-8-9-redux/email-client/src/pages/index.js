import HomePage from "./Home";
import InboxPage from "./Inbox";
import ArchivePage from "./Archive";
import EmailPages from "./EmailPages";
import LoginPage from "./LoginPage";
import PrivateRout from "./PrivateRout";

export { HomePage, InboxPage, ArchivePage, EmailPages, LoginPage, PrivateRout};
