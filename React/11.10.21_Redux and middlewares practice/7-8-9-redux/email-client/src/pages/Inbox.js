import React, { useState, useEffect, } from "react";
import MailsList from "../components/MailsList/MailsList";
import { useSelector } from "react-redux";


const InboxPage = (props) => {
  

  const isLoading = useSelector((state)=>{
    return state.isLoading;
  })

  const emails = useSelector((state) => {
    const emails = state.emails;
    const inboxEmails = emails.filter(({ isRead }) => !isRead);
    return inboxEmails;
  })



  console.log("InboxPage:", props);
  return (
    <div>
      {isLoading ? (
        <p>Loading..</p>
      ) : (
        <>
          <MailsList title="Inbox" mails={emails} />
        </>
      )}
    </div>
  );
};

export default InboxPage;
