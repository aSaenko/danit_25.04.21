import React, { useEffect } from "react";
import {useDispatch} from "react-redux"
import { Link } from "react-router-dom";
import {FaStar} from 'react-icons/fa'
import {setFavorite} from "../../../store/actions"

const MailItem = props => {

    const dispatch = useDispatch()
    const mailto = `mailto:${props.from}`;
    let path = window.location.pathname;

    return(
        <li>
            <h2>Topic: {props.topic}</h2>
            <FaStar color={props.isFavorite ? 'orange' : 'black'}  size={30} onClick={()=>{dispatch(setFavorite(props.id))}} />
            From: <a  href={mailto}>
            {props.from}
            </a>
           <br /> 
           
           <Link to={`${path}/${props.id}`}>Read more</Link>
        </li>
    )
}

export default MailItem;