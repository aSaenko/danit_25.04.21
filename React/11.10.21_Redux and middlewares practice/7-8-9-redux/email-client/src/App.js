import React from "react";
import { Switch, Route } from "react-router-dom";
import { HomePage, ArchivePage, InboxPage, EmailPages, LoginPage, PrivateRout } from "./pages";
import Header from "./components/Header";

const App = () => (
    <React.Fragment>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/archive" component={ArchivePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/inbox/:id" component={EmailPages} />
        <Route path="/archive/:id" component={EmailPages} />
        <PrivateRout exact to='/inbox'><InboxPage /> </PrivateRout>
      </Switch>
    </React.Fragment>
  )

export default App;
