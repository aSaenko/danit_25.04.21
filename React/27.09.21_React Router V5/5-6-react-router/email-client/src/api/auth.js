const USER_DB = [           // перечень наших узеров
    {   name: "John" ,
        password: "qwerty" ,
        role: "admin",
    },

    {   name: "alex" ,
        password: "123" ,
        role: "user",
    },

    {   name: "Ujin" ,
        password: "456" ,
        role: "manager",
    },
]

class Auth {   // логика: если совпадает то пропустит.
    isAuth = false;

    getAuthStatus(){
        return this.isAuth;
    }

    singIn(userName,password){
        const currentUser = USER_DB.find(({name})=> name === userName); // ищет имя пользователя

        if (!currentUser) return false;

        this.isAuth = currentUser.password === password;

        return this.isAuth;
    }
}

const instance = new Auth(); // где бы мы не вызвали всегда будет с тем же апи

export default instance;