import HomePage from "./Home";
import InboxPage from "./Inbox";
import ArchivePage from "./Archive";
import EmailPages from "./EmailPages";

export { HomePage, InboxPage, ArchivePage, EmailPages };
