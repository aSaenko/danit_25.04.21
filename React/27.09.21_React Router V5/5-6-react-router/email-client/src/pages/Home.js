import React, { useState, useEffect } from "react";
import authApi from "../api/auth";


const HomePage = () => {

  return <h1>Hello {authApi.getAuthStatus()?"friend":"guest"}</h1>;
};

export default HomePage;
