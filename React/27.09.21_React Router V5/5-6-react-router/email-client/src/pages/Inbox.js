import React, { useState, useEffect } from "react";
import MailsList from "../components/MailsList/MailsList";
import { getEmails } from "../api/emails";

const InboxPage = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [emails, setEmails] = useState([]);

  useEffect(() => {
    getEmails()
      .then((emails) => {
        setEmails(emails);
        localStorage.setItem("emails", JSON.stringify(emails));
      })
      .finally(() => setIsLoading(false));
  }, []);

  const inboxEmails = emails.filter(({ isRead }) => !isRead);

  console.log("InboxPage:", props);
  return (
    <div>
      {isLoading ? (
        <p>Loading..</p>
      ) : (
        <>
          <MailsList title="Inbox" mails={inboxEmails} />
        </>
      )}
    </div>
  );
};

export default InboxPage;
