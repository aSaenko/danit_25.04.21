import React, { useEffect } from "react";
import { Link } from "react-router-dom";

const MailItem = props => {

    
    const mailto = `mailto:${props.from}`;
    let path = window.location.pathname;

    return(
        <li>
            <h2>Topic: {props.topic}</h2>
            From: <a  href={mailto}>
            {props.from}
            </a>
           <br /> 
           
           <Link to={`${path}/${props.id}`}>Read more</Link>
        </li>
    )
}

export default MailItem;