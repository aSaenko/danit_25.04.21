import React from "react";
import MailItem from "./components/MailItem";

class MailsList extends React.Component{

    render(){
const clickHandler = !!this.props.onClick ? id => () => this.props.onClick(id) : null;   

        return (
            <div>
                <h2>{this.props.title}</h2>
                <ol>
                    {this.props.mails.map(mail => <MailItem 
                        key={mail.id} 
                        from={mail.from} 
                        topic={mail.topic} 
                        id={mail.id}
                       
                        />)}
                </ol>
            </div>
        )
    }
}



export default MailsList;