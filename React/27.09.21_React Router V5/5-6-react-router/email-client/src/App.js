import React from "react";
import { Switch, Route } from "react-router-dom";
import authApi from "./api/auth";

import { HomePage, ArchivePage, InboxPage, EmailPages } from "./pages";
import Header from "./components/Header";
import LoginPage from "./pages/LoginForm";
import PleaseLoginPage from "./pages/PleaseLoginPage";
import PrivateRout from "./pages/PrivateRout";
const App = () => {
    // window.AUTH_API = authApi; // так можно будет поиграться переключением зареган или нет в консоле.

    return(
        <React.Fragment>
            <Header />
            <Switch>
                <Route exact path="/" component={HomePage} />
                <PrivateRout to="/inbox"  /><InboxPage/>
                <Route exact path="/inbox" render={() => authApi.getAuthStatus() ? <InboxPage/> : <PleaseLoginPage/>} />
                <Route exact path="/archive" component={ArchivePage} />
                <Route path="/login" component={LoginPage} />
                <Route path="/inbox/:id" component={EmailPages} />
                <Route path="/archive/:id" component={EmailPages} />
            </Switch>
        </React.Fragment>
    );
}
export default App;
