import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import authApi from "../api/auth";
import PleaseLoginPage from '../pages/PleaseLoginPage';


export default ({props, children}) => {
    return (<Route {...props} >{authApi.getAuthStatus() ? children : <Redirect to="/login" /> }</Route>);
}