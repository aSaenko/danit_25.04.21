import React from 'react';
import LoginForm from './LoginForm';

export default () => (<div>
    <h1>Login form</h1>
    <LoginForm />
</div>);