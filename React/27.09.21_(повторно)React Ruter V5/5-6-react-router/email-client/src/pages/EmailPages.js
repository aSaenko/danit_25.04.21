import React from "react";

let EmailPages = (props) => {
  console.log(props)
  let {
    match: {
      params: { id },
    },
  } = props;
  let emailsString = localStorage.getItem("emails");
  if (!emailsString || !id) {
    return <h1>mail not found</h1>;
  }

  let emails = JSON.parse(emailsString);
  let email = emails.filter(({ id: curentId }) => id == curentId);
  if (!email) {
    return <h1>mail not found</h1>;
  }
  console.log({ email, emails });
  return (
    <div>
      <h1>Topic:{email[0].topic}</h1>
      <h2>From:{email[0].from}</h2>
      <p>{email[0].title}</p>
      <p>{email[0].text}</p>
    </div>
  );
};
export default EmailPages;
