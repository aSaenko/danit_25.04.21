import React from "react";
import { Link } from "react-router-dom";

//навигация ! меню
class Header extends React.Component {
    render(){
        return (
            <div className="header">
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/inbox">Inbox</Link></li>
                    <li><Link to="/archive">Archive</Link></li>
                    <li><Link to="/login">sign in</Link></li>
                </ul>
             
             
             
                <h1>Email client</h1>
                {this.props.children}
            </div>
        )
    }
}

export default Header;