import emails from '../fixtures/emails'

export const getEmails = () => new Promise((resolve, reject) => {
    resolve(emails)
})
