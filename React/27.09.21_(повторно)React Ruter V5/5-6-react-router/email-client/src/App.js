import React from "react";
import { Switch, Route } from "react-router-dom";
import authApi from "./api/auth";

import { HomePage, ArchivePage, InboxPage, EmailPages } from "./pages";
import Header from "./components/Header";
import LoginPage from "./pages/LoginPage";
import PleaseLoginPage from "./pages/PleaseLoginPage";
import PrivateRout from "./pages/PrivateRout";

const App = () => {

  return (
  <React.Fragment>
    <Header />
    <Switch>
      <Route exact path="/" component={HomePage} />
      
      {/* <Route exact path="/inbox" render={() => authApi.getAuthStatus() ? <InboxPage /> : <PleaseLoginPage />} /> */}
      <Route exact path="/archive" component={ArchivePage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/inbox/:id" component={EmailPages} />
      <Route path="/archive/:id" component={EmailPages} />
      <PrivateRout exact to='/inbox'><InboxPage/> </PrivateRout>
    </Switch>
  </React.Fragment>
)};
export default App;
