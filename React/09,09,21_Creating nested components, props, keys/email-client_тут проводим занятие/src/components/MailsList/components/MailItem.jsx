import React from "react";
//берем с сервера данные обьекта
class MailItem extends React.Component{
    render() {
        const mailto = `mailto: ${this.props.from}`


        return (
            <li>
                <h2>Topic: {this.props.topic}</h2>
                From: <a href={mailto}>{this.props.from}</a>
                <br/> {!!this.props.onClick && <button onClick={this.props.onClick}>Add to archive</button>}
            </li>
        );
    }

}

export default MailItem;