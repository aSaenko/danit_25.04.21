import React from "react";
import MailItem from "./components/MailItem";
// import propTypes from "prop-types";

class MailsList extends React.Component{

    // жизненый цикл компонентов
    // componentDidMount() {
    //     console.log("hello,i did mount!", this)
    // }

    // componentWillReceiveProps(nextProps) {
    //     console.log('##########################################');
    //     console.log("current props", this.props);
    //     console.log("next props", nextProps);
    //     console.log('##########################################');
    // }

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     if (this.props.title === "Inbox" || nextProps.title === "Inbox")return false;
    //
    //     return true;
    // }

    render() {
        const clickHandler = !!this.props.onClick ? id =>() =>this.props.onClick(id) :null;

        return (
            <div>
                <h2>{this.props.title}</h2>
                <ol>
                    {this.props.mails.map(mail => <MailItem
                        key={mail.id}
                        from={mail.from}
                        topic={mail.topic}
                        onClick={clickHandler && clickHandler(mail.id)}/>)}

                </ol>
            </div>
        )
    }
}
// покажет что мы передали не так
// MailsList.PropTypes = {
//     title: propTypes.string,
//     mails: propTypes.array,
//     onClick: propTypes.func,
// }
// MailsList.defaultProps = {
//     title: 'DEFAULT TITLE'
// }


export default MailsList;