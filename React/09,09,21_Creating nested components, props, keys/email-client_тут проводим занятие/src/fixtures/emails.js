const getEmails = ()=> new Promise(function (resolve){
  setTimeout(function (){
    resolve([
      { id: 12, topic: "Rozetka", from: "rozetka@gmail.com", isRead: false },
      { id: 43, topic: "Citrus", from: "citrus@gmail.com", isRead: false },
      { id: 54, topic: "Prom.ua", from: "promua@gmail.com", isRead: false },
      { id: 23, topic: "Amazon", from: "amazon@gmail.com", isRead: true },
      { id: 46, topic: "Ali", from: "ali@gmail.com", isRead: true },
    ])
  },300)
})

export default getEmails;