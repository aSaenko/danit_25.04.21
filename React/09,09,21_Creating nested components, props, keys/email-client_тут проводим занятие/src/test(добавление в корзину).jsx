import React, {useState} from "react";

const shopItems = [
    {id: id(), name: 'Сыр', price: 100, inCart: false},
    {id: id(), name: 'Хлеб', price: 200, inCart: false},
    {id: id(), name: 'Молоко', price: 300, inCart: false},
];

function addToCart(id) {
    setProds(prods.map(prod => {
        if (prod.id === id) {
            prod.inCart = true;
        }
        return prod;
    }));
}

function Products() {
    const [prods, setProds] = useState(shopItems);

    const result = prods.map(prod => {
        return <Product
            key={prod.id}
            id={prod.id}
            name={prod.name}
            price={prod.price}
            inCart={prod.inCart}
            addToCart={addToCart} />;
    });

    return {result};
}

function Product({ id, name, price, inCart, addToCart }) {
    return <div>
        Товар: {name},
        Цена: {price},
        <div>{inCart ? 'В корзине' : ''}</div>,
        <button onClick={() => addToCart(id)}>Добавить в корзину</button>
    </div>;
}