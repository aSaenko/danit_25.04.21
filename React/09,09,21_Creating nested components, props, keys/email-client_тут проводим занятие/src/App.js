import React from "react";
// import "./App.scss";
import Header from "./components/Header";
import MailList from "./components/MailsList/MailList";
import getEmails from "./fixtures/emails";
import emails from "./fixtures/emails";
import Loading from "./components/Loading";


// тут происходить весь конечный рендер и соединение все компонентов
class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {emails:[],isLoading:false};   // добавили состояние
        this.addToArchive= this.addToArchive.bind(this);
    }
    // в нашем случаее работает как fetch запрос на сервер
    componentDidMount() {
        this.setState({ isLoading: true });
        getEmails()
            .then((data) => {
                this.setState({ emails: data });
            })
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }


    // componentWillUpdate(nextProps, nextState) {
    //     console.log("current props", this.props);
    //     console.log("next props", nextProps);
    //     console.log("current state", this.state);
    //     console.log("next state", nextState);
    // }


    //еще одно состояние для нопки, что бы добалять в архив конкретный обьект
    addToArchive(id){
        const updatedEmails = this.state.emails.map((mail)=>{
            if (id === mail.id){
                return {...mail,isRead : true}
            }
            return mail;
        });
        this.setState({emails:updatedEmails});
    }
  render() {
        const inboxEmails = this.state.emails.filter(({isRead})=>!isRead);  // ищет и проверяет прочитано письмо или нет
      const readedEmails = this.state.emails.filter(({isRead})=>!!isRead);

      if (this.state.isLoading) return <Loading />;
      return (

         <React.Fragment>
           <Header />
             <MailList title="Inbox" mails={inboxEmails} onClick={this.addToArchive}/>
             <MailList title="Archive" mails={readedEmails}/>
         </React.Fragment>
    )
  }
}

export default App;
