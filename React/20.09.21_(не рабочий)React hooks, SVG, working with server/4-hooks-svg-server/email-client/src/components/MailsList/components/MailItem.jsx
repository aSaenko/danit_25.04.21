import React, { useEffect } from "react";

const MailItem = props => {
    useEffect(() => {
        console.log('mounted');
        return () => {
            alert('unmount')
        }
    }, [])
    
    const mailto = `mailto:${props.from}`;

    return(
        <li>
            <h2>Topic: {props.topic}</h2>
            From: <a  href={mailto}>
            {props.from}
            </a>
           <br /> {!!props.onClick && <button onClick={props.onClick}>Add to archive</button>}
        </li>
    )
}

/* class MailItem extends React.Component{
    render(){
const mailto = `mailto:${this.props.from}`;
// console.log(this.props.onClick)
        return(
            <li>
                <h2>Topic: {this.props.topic}</h2>
                From: <a  href={mailto}>
                {this.props.from}
                </a>
               <br /> {!!this.props.onClick && <button onClick={this.props.onClick}>Add to archive</button>}
            </li>
        )
    }
} */

export default MailItem;