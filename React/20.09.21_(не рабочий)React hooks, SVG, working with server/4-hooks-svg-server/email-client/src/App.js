import React, { useEffect, useState } from "react";
import { getEmails } from "./api/emails";
// import "./App.scss";
import Header from "./components/Header";
import MailsList from "./components/MailsList/MailsList";
// import emails from "./fixtures/emails";


const App = props => {
  const [isLoading, setIsLoading] = useState(true)
  const [emails, setEmails] = useState([])

  useEffect(() => {
    getEmails()
      .then(emails => setEmails(emails))
      .finally(() => setIsLoading(false))
  }, [])

  useEffect(() => {
    // alert('emails are changed')
  }, [emails])

  const addToArchive = id => {
    const updatedEmails = emails.map((mail) => {
      if (id === mail.id) {
        return { ...mail, isRead: true };
      }
      return mail;
    });
    setEmails(updatedEmails)
  }

  const inboxEmails = emails.filter(({ isRead }) => !isRead);
  const readedEmails = emails.filter(({ isRead }) => !!isRead);
  return (
    <React.Fragment>
      <Header />
      {isLoading ? (
        <p>Loading..</p>
      ) :
        (
          <>
            <MailsList
              title="Inbox"
              mails={inboxEmails}
              onClick={addToArchive}
            />
            <MailsList title="Archive" mails={readedEmails} />
          </>
        )}
    </React.Fragment>
  )
}

/* class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { emails: [], isLoading: true };
    this.addToArchive = this.addToArchive.bind(this);
  }

  componentDidMount() {
    getEmails()
      .then(emails => {
        this.setState({ emails })
      })
      .finally(() => this.setState({ isLoading: false }))
  }

  addToArchive(id) {
    const updatedEmails = this.state.emails.map((mail) => {
      if (id === mail.id) {
        return { ...mail, isRead: true };
      }

      return mail;
    });
    this.setState({ emails: updatedEmails });
  }

  render() {
    const inboxEmails = this.state.emails.filter(({ isRead }) => !isRead);
    const readedEmails = this.state.emails.filter(({ isRead }) => !!isRead);
    return (
      <React.Fragment>
        <Header />
        {this.state.isLoading ? (
          <p>Loading..</p>
        ) :
          (
            <>
              <MailsList
                title="Inbox"
                mails={inboxEmails}
                onClick={this.addToArchive}
              />
              <MailsList title="Archive" mails={readedEmails} />
            </>
          )}
      </React.Fragment>
    );
  }
} */

export default App;
