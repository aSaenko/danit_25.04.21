import React, {useState} from 'react';

const ListItem = ({film}) => {
const [showMore, setShowMore] = useState(false);
const handleClick = () => {
    setShowMore(true);
}
    return (
        <li key={film.id}>{film.name} {!showMore ? <button onClick={handleClick}>Show more</button> : <span>{film.episodeId}</span>}</li>
    )
}

export default ListItem;