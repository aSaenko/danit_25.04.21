// import logo from './logo.svg';
import React, { useState, useEffect } from 'react'
import './App.css';
import { getFilms } from './api/films'
import List from './components/list/List'
function App() {
  const [films, setFilms] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    getFilms()
      .then(data => { setFilms(data) })
      .finally(() => { setIsLoading(false) })
  }, []
  )




  return (
    <>
      <h1>Swapi films app</h1>
      {isLoading ? <p>Loading...</p> :
        <List filmList={films} />}
    </>
  );
}

export default App;
