import React from "react";
import Post from "./Post";

class PostsList extends React.Component {
    render(){
        return(
            <ul>
                {this.props.posts.map(post => (
                    <Post key={post.id} title={post.title} text={post.body} />
                ))}
            </ul>
        )
    }
}

export default PostsList