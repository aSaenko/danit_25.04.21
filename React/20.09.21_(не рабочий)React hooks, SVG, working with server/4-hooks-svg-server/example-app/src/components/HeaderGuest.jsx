import React from "react";
import Header from "./Header";

class HeaderGuest extends React.Component {
    render(){
        return(
            <Header> <a href="#" onClick={this.props.onClick}>Log in</a></Header>
        )
    }
}

export default HeaderGuest;