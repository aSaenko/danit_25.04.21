export const getFilms = () => {
    return fetch('https://ajax.test-danit.com/api/swapi/films').then((response) => response.json())
}