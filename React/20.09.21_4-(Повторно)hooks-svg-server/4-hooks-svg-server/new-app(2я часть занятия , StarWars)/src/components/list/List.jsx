import ListItem from './listItem/ListItem';
const List = (props) =>{
   const list = props.filmList.map(film => {   // перебирает возвращает по ид
        return(
            <ListItem key={film.id} film={film}/>
            
        )
    })
    return (
        <ol>{list}</ol>
    )
}

export default List;