import React, {useState} from 'react';

const ListItem = ({film}) => {  // фильм принимаем с перебора мапом
const [showMore, setShowMore] = useState(false); // стейт для кнопки шоу мор
const handleClick = () => {
    setShowMore(true);
}
    return (
        <li key={film.id}>{film.name} {!showMore ? <button onClick={handleClick}>Show more</button> : <span>{film.episodeId}</span>}</li>
    )
}

export default ListItem;