import React, { Component } from 'react';

// function Hello(props){
//         const {
//             user,
//             showLastName
//         } = props

//         return(
        // <div>
        //   <h1>Hi {user.firstName} {showLastName && user.lastName}!</h1>
        // </div>
//         )
// }

const Hello = ({user: {firstName, lastName}, showLastName}) => (
  <div>
    <h1>Hi {firstName} {showLastName && lastName}!</h1>
  </div>
);

/* class Hello extends Component {
    render() {
        console.log('props:', this.props);
        const {
            user,
            showLastName
        } = this.props

      return (
        <div>
          <h1>Hi {user.firstName} {showLastName && user.lastName}!</h1>
        </div>
      );
    }
  }
 */
  export default Hello;