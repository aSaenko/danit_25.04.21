import React, { Component } from 'react';
import '../style/theme.css'

class ToggleTheme extends Component {
    // state = {
    //     isDark: true
    // }

    constructor(){
        super();

        this.state = {
            isDark: true
        }

        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    handleToggleClick(){
        this.setState({
            isDark: !this.state.isDark
        })
    }

    render() {
      const themeClassName = this.state.isDark ? 'dark' : '';
      const styles = {
        backgroundColor: 'red',
        color: 'white'
    }

      return <div>
          <span>{this.state.isDark ? "Dark" : 'Light'}</span>
          <button className={themeClassName}
          style={styles}
          onClick={this.handleToggleClick}>Toggle theme</button>
      </div>;
    }
  }

  export default ToggleTheme;