import React from "react";
import Header from "./Header";

class HeaderUser extends React.Component {
    render(){
        return(
            <Header>
                <ul>
                    <li>Home</li>
                    <li>About us</li>
                    <li>Contact</li>
                </ul>
            </Header>
        )
    }
}

export default HeaderUser;