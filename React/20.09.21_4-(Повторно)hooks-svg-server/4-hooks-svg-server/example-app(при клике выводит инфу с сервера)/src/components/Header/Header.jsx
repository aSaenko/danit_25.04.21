import React from 'react';


const Header = ({ children }) => {
    return (
        <div className="header">
            <h1>LOGO</h1>
            <hr />
            {children}
        </div>
    )
}

/* class Header extends React.Component {
    render(){
        return(
            <div className="header">
                <h1>LOGO</h1>
                <hr />
                {this.props.children}
            </div>
        )
    }
} */

export default Header;