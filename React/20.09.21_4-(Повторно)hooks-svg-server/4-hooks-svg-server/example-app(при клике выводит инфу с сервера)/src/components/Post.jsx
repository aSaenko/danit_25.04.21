import React from 'react';

class Post extends React.Component {
    render(){
        console.log(this.props)
        return(
            <li>
                <h2>{this.props.title}</h2>
                <p style={{ border: '1px solid black'}}>
                    {this.props.text}
                </p>
            </li>
            
        )
    }
}

export default Post;