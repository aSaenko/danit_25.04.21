import React, { useState } from "react";

import PostsList from "./components/PostsList";
import HeaderUser from "./components/HeaderUser";
import HeaderGuest from "./components/HeaderGuest";

import posts from "./fixtures/posts";
import Hello from "./components/Hello";

// все тоже самое что и с классовыми только в аргументах мы передаем props
const App = props =>{

    const [isLogin, setIsLogin] = useState(false) // этим создаем состояние
    // const isLogin = false;
    const Header = isLogin ? HeaderUser : HeaderGuest;
    const user = { firstName: 'John', lastName: 'Doe' }

    const logIn = (event)=>{   // этим меняем стейт
        event.preventDefault();
        setIsLogin(true)
    }

    return (
        <div>
            <Header onClick={logIn} />
            <Hello user={user} showLastName={true} />
            <PostsList posts={posts} />
        </div>
    );
}







// const App = props => {
//   const [isLogin, setIsLogin] = useState(false)
//
//   // console.log(isLogin);
//   // const isLogin = false
//   const Header = isLogin ? HeaderUser : HeaderGuest;
//   const user = { firstName: 'John', lastName: 'Doe' }
//
//   const logIn = (event) => {
//     event.preventDefault();
//     setIsLogin(true)
//   }
//
//   return (
//     <div>
//       <Header onClick={logIn} />
//       <Hello user={user} showLastName={true} />
//       <PostsList posts={posts} />
//     </div>
//   );
// }

/* class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLogin: false,
    };

    this.logIn = this.logIn.bind(this);
  }

  logIn(e) {
    e.preventDefault();

    this.setState({ isLogin: true });
  }

  render() {
    const Header = this.state.isLogin ? HeaderUser : HeaderGuest;
    const user = { firstName: 'John', lastName: 'Doe' }
    return (
      <div>
        <Header onClick={this.logIn} />
        <Hello user={user} showLastName={true} />
        <PostsList posts={posts} />
      </div>
    );
  }
} */

export default App;
