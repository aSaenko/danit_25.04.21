const express = require('express');

const app = express();

const port = 8085

const emails = [
   { id: 12, topic: 'Rozetka', from: 'rozetka@gmail.com', isRead: true },
   { id: 43, topic: 'Citrus', from: 'citrus@gmail.com', isRead: true },
   { id: 54, topic: 'Prom.ua', from: 'promua@gmail.com', isRead: false },
   { id: 23, topic: 'Amazon', from: 'amazon@gmail.com', isRead: true },
   { id: 46, topic: 'Ali', from: 'ali@gmail.com', isRead: false },
]

app.get('/api/emails', (req, res) => {
   res.send(emails)
})

app.get('/api/emails/:emailId', (req, res) => {
   res.send(emails.find(e => e.id === +req.params.emailId))
})

app.listen(port, () => {
   console.log(`Server listening on port ${port}`)
})