import React from "react";


class Header extends React.Component {
    render(){
        return (
            <div className="header">
                <h1>Email client</h1>
                {this.props.children}
            </div>
        )
    }
}

export default Header;