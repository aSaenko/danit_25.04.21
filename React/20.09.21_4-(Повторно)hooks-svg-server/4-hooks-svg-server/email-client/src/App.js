import React, { useEffect, useState } from "react";
import { getEmails } from "./api/emails";
// import "./App.scss";
import Header from "./components/Header";
import MailsList from "./components/MailsList/MailsList";
// import emails from "./fixtures/emails";

// повторный пересмотр , это писал сам

const App = props =>{
    const [isLoading, setIsLoading] = useState(false)
    const [emails ,setEmails] = useState([])

    useEffect(()=>{   // этим методом заменяем compDidMount , им делается запрос на сервер
        getEmails()
            .then(emails => setEmails(emails))
            .finally(() => setIsLoading(false ))
    },[])  // массив показывает что он будет выполнятся 1 раз  , если там что то будет то это будет означать что то что мы туда впишем за тем и будет смотреть юзэфект , смотреть за его изменениями , то есть когда увидит изминения сделает это



    const addToArchive = id =>{
            const updatedEmails = emails.map((mail) => {
                if (id === mail.id) {
                    return {...mail, isRead: true};
                }
                return mail
            });
        setEmails(updatedEmails);
    }

    const inboxEmails = emails.filter(({ isRead }) => !isRead);
    const readedEmails = emails.filter(({ isRead }) => !!isRead);
    return(
        <React.Fragment>
            <Header />
            {isLoading ? (    // этим и прописываем что при медленной загрузке выводи параграф с лоадинг , а как загрузится то выводи контент
                    <p>Loading..</p>
                ) :
                (
                    <>
                        <MailsList
                            title="Inbox"
                            mails={inboxEmails}
                            onClick={addToArchive}
                        />
                        <MailsList title="Archive" mails={readedEmails} />
                    </>
                )}
        </React.Fragment>
    )
}



// const App = props => {
//   const [isLoading, setIsLoading] = useState(true)
//   const [emails, setEmails] = useState([])
//
//   useEffect(() => {
//     getEmails()
//       .then(emails => setEmails(emails))
//       .finally(() => setIsLoading(false))
//   }, [])
//
//   useEffect(() => {
//     // alert('emails are changed')
//   }, [emails])
//
//   const addToArchive = id => {
//     const updatedEmails = emails.map((mail) => {
//       if (id === mail.id) {
//         return { ...mail, isRead: true };
//       }
//       return mail;
//     });
//     setEmails(updatedEmails)
//   }
//
//   const inboxEmails = emails.filter(({ isRead }) => !isRead);
//   const readedEmails = emails.filter(({ isRead }) => !!isRead);
//   return (
//     <React.Fragment>
//       <Header />
//       {isLoading ? (
//         <p>Loading..</p>
//       ) :
//         (
//           <>
//             <MailsList
//               title="Inbox"
//               mails={inboxEmails}
//               onClick={addToArchive}
//             />
//             <MailsList title="Archive" mails={readedEmails} />
//           </>
//         )}
//     </React.Fragment>
//   )
// }

/* class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { emails: [], isLoading: true };
    this.addToArchive = this.addToArchive.bind(this);
  }

  componentDidMount() {   // этим стучим на наш локальный сервер
    getEmails()
      .then(emails => {
        this.setState({ emails })
      })
      .finally(() => this.setState({ isLoading: false })) // это написано для лоадинг , файнали для того что бы в любом случае выходило
  }

  addToArchive(id) {
    const updatedEmails = this.state.emails.map((mail) => {
      if (id === mail.id) {
        return { ...mail, isRead: true };
      }

      return mail;
    });
    this.setState({ emails: updatedEmails });
  }

  render() {
    const inboxEmails = this.state.emails.filter(({ isRead }) => !isRead);
    const readedEmails = this.state.emails.filter(({ isRead }) => !!isRead);
    return (
      <React.Fragment>
        <Header />
        {this.state.isLoading ? (    // этим и прописываем что при медленной загрузке выводи параграф с лоадинг , а как загрузится то выводи контент
          <p>Loading..</p>
        ) :
          (
            <>
              <MailsList
                title="Inbox"
                mails={inboxEmails}
                onClick={this.addToArchive}
              />
              <MailsList title="Archive" mails={readedEmails} />
            </>
          )}
      </React.Fragment>
    );
  }
} */

export default App;
