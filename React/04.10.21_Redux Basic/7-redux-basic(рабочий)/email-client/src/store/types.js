const MINUSE = 'MINUSE';
const PLUSE = 'PLUSE';
const SET_COUNTER = 'SET_COUNTER';

export default {
    MINUSE, 
    PLUSE,
    SET_COUNTER
}