import types from "./types";

export const pluseCounter = () => ({
    type: types.PLUSE
})

export const minuseCounter = () => ({ type: types.MINUSE})

export const setCounter = (value) => ({ type: types.SET_COUNTER, payload: value })