import types from './types'

const initialState = {
    counter: 0
}

function reducer (state = initialState,action){
    switch(action.type) {
        case types.SET_COUNTER:
            return {
                ...state,
                counter: action.payload
            }
        case types.PLUSE:
            return {
                ...state, counter: state.counter+1
            }

            case types.MINUSE:
                return {
                    ...state, counter: state.counter-1
                }

                default:
                    return {
                    ...state
                }
    }
}


// reducer({counter: 2}, { type: 'SET_COUNTER', payload: 11})

export default reducer;