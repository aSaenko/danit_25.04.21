const USER_DB = [
    {
     name: "John",
     password: "qwerty",
     role: "admin"
    },

    {
        name: "Alex",
        password: "123",
        role: "user"
       },

       {
        name: "Ujin",
        password: "456",
        role: "manager"
       }
]

class Auth{
    isAuth = false;

    getAuthStatus(){
        return this.isAuth;
    }

    signIn(userName, password){
        const currentUser = USER_DB.find(({name}) => name === userName);

        if(!currentUser) return false;

        this.isAuth = currentUser.password === password;

        return this.isAuth;
    }
}

const instance = new Auth();

export default instance;