import React, { useState, useEffect } from "react";
import {useSelector, useDispatch} from "react-redux"
import authApi from "../api/auth";
import {pluseCounter, minuseCounter, setCounter} from "../store/actions"


const ViewA = () => {
  const counterA = useSelector(state => state.counter);
  return (<h2>VIEW A: {counterA}</h2>)
}

const ViewB = () => {
  const counterB = useSelector(state => state.counter);
  return (<h2>VIEW B: {counterB}</h2>)
}

const HomePage = () => {
  const initalCounter = useSelector(state => state.counter);
  // const [counter, setCounter] = useState(initalCounter);

  const dispatch = useDispatch()

  return <div>
    <h1>Hello {authApi.getAuthStatus() ? "friend" : "guest"}</h1>
    counter:{initalCounter}
    <button onClick={() => dispatch(pluseCounter())}> + </button>
    <button onClick={() => dispatch(minuseCounter())}> - </button>
    <button onClick={() => dispatch(setCounter(11))}>set counter value 11</button>
    <hr/>
    <ViewA />
    <ViewB />
    </div>;
};

// dispatch({ type: 'SET_COUNTER', payload: 11})

export default HomePage;
