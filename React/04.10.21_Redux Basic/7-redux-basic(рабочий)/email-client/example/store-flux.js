// ACTION
// {
//     type: "SET_COUNTER",
//     payload: 123
// }

function rootReducer(state, action) {
    switch (action.type) {
        case 'SET_COUNTER':
            return { ...state, counter: state.counter + action.payload}
        case 'RESET_COUNTER':
            return { ...state, counter: 0};
        default: return { ...state}
    }
}

const initialState = { counter: 0 }

function createStore(reducer, initialState){
    let state = initialState;

    return {
        getStore: () => state,
        dispatch: (action) => { 
            state = reducer(state, action)
        }
    }
}

const counterStore = createStore(rootReducer, initialState);

module.exports = counterStore;