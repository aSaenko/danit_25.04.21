const counterStore = require('./store-flux')

console.log('store[0]: ', counterStore.getStore())

counterStore.dispatch({
    type: 'SET_COUNTER',
    payload: 777
})

console.log('store[1]: ', counterStore.getStore())

setTimeout(() => {
    counterStore.dispatch({ type: 'RESET_COUNTER' })

    console.log('store[2]: ', counterStore.getStore())
}, 5000)

