import "./App.css";
import {
  BrowserRouter,
  HashRouter,
  MemoryRouter,
  Route,
  Switch,
  Link,
} from "react-router-dom";

const Home = () => <h2>Home</h2>;

const About = () => <h2>About</h2>;

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <Link to="/">Home</Link>
          <Link to="/about">About</Link>
        </header>
        <div>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route path="/about">
              <About />
            </Route>
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
