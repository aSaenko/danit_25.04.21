
// на основ1 масиву потр1бно створити новий масив
// який м1стить т1льки дати п1сля 1 с1чня 2000 року

const  dateArr = [
  "2011-3-4",
  "2017-7-22",
  "1980-6-17",
  "2020-5-18",
  "1992-1-27",
];

const filterDate = new Date("2000-01-01"); // дата от которой мы отталкиваемся

const filterArr = dateArr.filter(dateStr =>{
  const date = new Date(dateStr);
  return date.getTime() > filterDate.getTime()
});
console.log(filterArr);