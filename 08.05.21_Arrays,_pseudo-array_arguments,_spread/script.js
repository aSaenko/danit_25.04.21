
/*
let styles = ["Джаз","Блюз"];  // создал
styles.push("Рок-н-рол");   // довавить в конец
styles[1] = "Класика";   // перезаписал
styles.shift();
styles.unshift("Рэп","Регги");

console.log(styles);
 */


/*
Напишите функцию sumInput(), которая:

Просит пользователя ввести значения, используя prompt и сохраняет их в массив.
    Заканчивает запрашивать значения, когда пользователь введёт не числовое значение, пустую строку или нажмёт «Отмена».
Подсчитывает и возвращает сумму элементов массива.
    P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0».
 */
/*
function sumInput() {

    let numbers = [];

    while (true) {

        let value = prompt("Введите число", 0);

        // Прекращаем ввод?
        if (value === "" || value === null || !isFinite(value)) break;

        numbers.push(+value);
    }

    let sum = 0;
    for (let number of numbers) {
        sum += number;
    }
    return sum;
}

alert( sumInput() );
 */

// classWork

//перезаписать конкретную ечейку- индекс - строку
/*
const day = ["Sunday","mondey","Tuesday"];

day[1] = "pineapple"; // добавление ,

delete day[2];   // удалить но и удаляет индекс (в основном не используется)
console.log(day);
 */

//    !!! методы массива !!!

// concat | arr1 + arr2
/*
let arr1 = ["apple","mango","orange"];
let arr2 = ["pineapple","melon"];
let arr3 = arr1.concat(arr2);

console.log(arr3);
 */

// split
/*
const fruits = "pineaple|water|milk|sausage";
console.log(fruits.split(","));  // если оставим пустой разобьет по буквам
 */


//  join
/*
console.log(["apple","mango"].join("/"));
 */

// push / pop
/*
let emptyArr = [];
emptyArr.push(1);
emptyArr.push(2);
emptyArr.push("Test");
console.log(emptyArr);
let laseElement = emptyArr.pop(); // без аргумента - значит удалит последниий
console.log(laseElement);
console.log(emptyArr);
 */

// unshift / shift

/*
let emptyArr = [];
emptyArr.unshift(1);
emptyArr.unshift(2);
console.log(emptyArr);
let lastElement = emptyArr.shift();
console.log(emptyArr);
console.log(lastElement);
*/

// splice
/*
arr = [1,2,3,4,5,6];
let value = arr.splice(2,2); // вроторе значение это длина
arr.splice(0,1, "element1","element2","element3"); // удалит и добавит новые элементы
console.log(value);
console.log(arr);
 */

// slice
/*
arr = [1,2,3,4,5,6];
const value = arr.splice(1,3);
console.log(arr);
console.log(value);
 */

//revers
/*
arr = [1,2,3,4,5,6];
arr.reverse();
console.log(arr);
//console.log("String".split("").reverse().join("")); // или так
 */

// includes , indexOf
/*
arr = ["apple","mango","orange"];
console.log(arr.indexOf("mango"));// покажет какой индекс
console.log(arr.includes("mango")); // искать есть или нет строкой, если есть такой обьект то будет true
 */

// sort
/*
arr = [1,2,15];
arr.sort();
//console.log(arr); // будет 1.15.2 изза юникода
arr.sort((a, b)=> {   // для того что бы правельно сортировало
    if (a > b) return 1;
    if (a == b ) return 0;
    if (a<b) return -1;
});

console.log(arr);
 */

//        !!!!!!!!!!!!!!!!!!    filter      !!!!!!!!!!!!
/*
arr = ["apple","mango","orange","melon","pineapple",1,2,3,{a:12},false];
/*let arrUpdated = arr.filter(
    (element) => element === "apple" || element === "pineapple"  // фильтрует по конктерному значению , выведет только о что мы хотим
);
// let arrUpdated = arr.filter((element) => element.length >5); // можн еще вот так
//let arrUpdated = arr.filter((element) => typeof element === "number");  // отфильтровать только числа
//let arrUpdated = arr.filter((element) => typeof element !== "number"); // получить все кроме чисел

console.log(arr);
console.log(arrUpdated);
 */

//          some   возращает true или false ,если будет хоть одно условие будет верным то будет тру
/*
arr = [1,2,3,"4",5];
let result = arr.some((element) => typeof element === "string");// говорит если есть хоть одна строка верни тру
console.log(result);
 */

//              !!!!!every - будет проверять все знаячение пока не увидит не коректное знаяение , грубо противополжное some
/*
arr = [1,2,3,"4",5];
let result = arr.some((element) => typeof element === "number");
console.log(result);
 */

//             forEach  перебор массива
/*
arr = ["apple","mango","orange","melon","pineapple"];
let result = arr.forEach((element, index, arr)=>
    console.log(index,element,arr)
);
 */
/*
const fn = (element, index , arr) => {
    alert("hello " + element);   // сюда можем подставить и индекс, будет показывать индекс каждоко элемента массива
};
arr = ["apple","mango","orange","melon","pineapple"];
let result = arr.forEach(fn);
 */



//               map  - для трансформации массива, этим исходный массив не меняется принимает функции

arr = [20, 40 ,12];
//let result = arr.map((item) => "converted-element");  // если оставлю так то выведет только эту надпись
let result = arr.map((item) => `converted-element - ${item + 2}`); // так задействует массив
console.log(result);


//                  !!!!!!!reduce / reduceRight   ожидает 2 аргумента  работает конвертатором
/*
arr = [20, 40 ,12];
let result = arr.reduce((acc, element , index)=> {
   acc[`name_${index}`] = element ;

   return acc;
}, {});      // эта строчка показывает это acc в него мы будем все добавлять его называют аккумулятором

console.log(arr);
console.log(result); // тут  увидем что наш массив  стал  обьектом
 */

//                                  !!!так просуморали
/*
arr = [20,40,12];
let result = arr.reduce((acc,element) => acc + element , 0);
console.log(result);
 */

//                                  !!!сделали реверс
/*
arr = [20, 40 ,12];
let result = arr.reduce((acc, element , index)=> {
   acc.unshift(element);

   return acc;
}, []);

console.log(arr);
console.log(result);
 */

// с строками
/*
arr = ["a","b","c"];
let result = arr.reduce((acc, element , index)=> {

    return acc + element;
}, "");        // что бы явно было видно что бы собираем
console.log(arr);
console.log(result);
 */


// !!!!!!!!!! ПРАКТИКА! ///

/*
данний масив з днями тижня
Вивести в конс вс1 дн1 тижня за допомогою;
вивести в консоль який сьогодн1 день тижня выкориставши масив
 */
/*
 const days = [
     "Sunday",
     "Monday",
     "Tuesday",
     "Wednesday",
     "Thursday",
     "Friday",
     "Saturday",
 ];

 let newDate = new Date();
let dayIndex = newDate.getDay();
console.log(days[dayIndex]);

/*
for (let i = 0; i <= days.length-1; i++){   // -1 для того что бы убрать ту фигню с отсчетом с 0 перебираем что бы вывести все что есть в массиве day
    console.log(days[i])
}
 */
/*
for (let i of days){  // еще один способ перебора
    console.log(i)
}
 */
/*
days.forEach(function (day) {  // а теперь способ перебора форычем
    console.log(day);
});
 */

// СТВОРИТИ З ДАННОГО СПИСКУ МАСИВ ТА ВИВЕСТИ ЙОГО В КОНСОЛЬ
/*
const list = "Tea; Yoghurt; Coffee; Milk; Rice; Water";

let newList = list.split("; ");
newList = newList.reverse();
//const reverseStr = newList.join("; ");  // соединяет обратно в строку
//console.log(reverseStr);


//                  вивести в консоль 2й 4й та 5й елемент
let arr = [null ,{name: "john"},200,100,["Kyiv","Lviv"],10,"Mango"];
console.log(arr[1],arr[3], arr[4]);

//                 додаты в масив
arr.push(35);
console.log(arr);
// перев1рити чи е в масив1 200 та 27
let result = arr.includes(200) || arr.includes(27);

console.log(result);

//                     обьеднати попередн1й масив з новим
const anotherArr = [400,"true","holiday",150 , undefined, null];
newList = arr.concat(anotherArr);
console.log(newList);

//створити масив який буде виводити  т1льки числов1 значенняя

const filterFn = (i) => typeof i === "number";
let filterArr = newList.filter(filterFn);
console.log(filterArr);

//                     помножити кожен елемент на 2

let mapFn = (i) => i * 2;
let mapArr = filterArr.map(mapFn); // map переберет а функция mapFN перемножит
console.log(mapArr);

 */


