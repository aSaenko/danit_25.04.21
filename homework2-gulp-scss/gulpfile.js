const gulp = require("gulp");
const sass = require("gulp-sass")(require('sass'));
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
const minifyCSS =require("gulp-css-minify");
const rename = require("gulp-rename");
const browserSync = require("browser-sync");


gulp.task("clean" , () =>{
    return gulp.src("dist" , {allowEmpty: true , read:false}).pipe(clean())
});

gulp.task("buildHtml" , () => {
    return gulp
        .src("src/*.html")
        .pipe(gulp.dest("dist"))
})

gulp.task("buildCss" , () => {
    return gulp
        .src("src/style/**/*.scss")
        .pipe(sass())
        .pipe(autoprefixer({
                overrideBrowserslist: ['last 100 versions'],
                cascade: false
            }))
        .pipe(minifyCSS())
        .pipe(rename("main.min.css"))
        .pipe(gulp.dest("dist"));
})

gulp.task("buildJs" , () => {
    return gulp
        .src("src/js/**/*.js")
        .pipe(gulp.dest("dist"))
})

gulp.task("buildImg" , () => {
    return gulp
        .src("src/img/**/*.*")
        .pipe(gulp.dest("dist/img"))
})

gulp.task("watch" ,() =>{
    browserSync.init({
        server: {
            baseDir: "dist"
        }
    });
    gulp.watch("src/**/*.*" ,
gulp.series("clean" , gulp.parallel("buildCss","buildHtml","buildJs","buildImg")))
})

