const burgerBtn = document.querySelector('.menu-btn');
const burgerMenu = document.querySelector('.nav-menu');
burgerBtn.addEventListener('click', function() {
    this.classList.toggle('open');
    burgerMenu.classList.toggle('active');
})