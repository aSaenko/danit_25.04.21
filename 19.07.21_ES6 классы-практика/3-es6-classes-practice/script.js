/**
 * Написати функціонал для тултипів (при наведении всплывает окно)
 *
 * Налаштування:
 * - текст
 * - позиціонування (зверху, справа, знизу, зліва), яке за замовчуванням зверху
 * - можливості відображення (при наведенні чи при кліку), за замовчуванням при наведенні
 *
 */

/**
 * <span class="tooltip-content">Some text</span>
 */

/**
 * placement - 'left', 'right', 'top' or 'bottom'
 * trigger - 'click', 'hover'
 */
const  POSITTIO_CLASS_NAME = new Map ([
    ["left" , "tooltip-left"],
    ["right","tooltip-right"],
    ["down","tooltip-down"],
    ["top", "tooltip-top"]
])
class Tooltip {
    disable = false;
    active = false;   // клас по умолчанию не активний

    constructor(selector , text,position = "up", event = "hover") {
        this.selector = selector;
        this.text = text;
        this.position = position;
        this.event = event;
    }
    init(){  // этим обьединили 2 метода
        this.render();
        this.attachEvent();
    }
    render() {  // отрисоввуем
        let root = document.querySelector(this.selector);  // ищем по селектору
        if (!root) return console.error("Root element not found!");
        root.classList.add("tooltip");
        root.classList.add(this.getPositionClassName());

        root.insertAdjacentHTML(   //  что именно розмещаем
            "afterbegin",
            `<span class="tooltip-content">${this.text}</span>`
        );
    }

    lock(){
        this.disable = true;
    }
    unlock(){
        this.disable = false;
    }
    show(){
        if (this.disable || this.active) return;
        let root = this.getRootElement();
        if (!!root){
            root.classList.add("active");
            this.active = true;
        }
    }
    hide(){
        if (!this.active || this.disable) return;
        let root = this.getRootElement();
        if (!!root) {
            root.classList.add("active");
            this.active = false;
        }
    }

    getRootElement(){   // что бы постоянно не исккать элемент
        let root = document.querySelector(this.selector);
        if (!root)  console.error("oot element not fount")
        return root;
    }
    attachEvent(){  // прикрепить событие
        let root = this.getRootElement();
        if (this.event === "click"){
            root.addEventListener("click" , this.toggleVisibility.bind(this)); // мистика , надо почитать
        }else {
            root.addEventListener("mouseover" , this.show.bind(this));
            root.addEventListener("mouseout" , this.hide.bind(this));
        }
    }

    toggleVisibility(){
        if(this.disable) return;

        let root = this.getRootElement();
        if (!!root) {
            root.classList.toggle("active")
            this.active =  !this.active;
        }
    }

    getPositionClassName(){   // куда розмещаем
        return POSITTIO_CLASS_NAME.has(this.position)
            ? POSITTIO_CLASS_NAME.get(this.position)
            : POSITTIO_CLASS_NAME.get("up");
    }
}

const  tooltip1 = new Tooltip("#btn-1" ,"It's first button" , 123 , "hover");
const  tooltip2 = new Tooltip("#btn-2" ,"It's second button" , "right" , "hover");
const  tooltip3 = new Tooltip("#btn-3" ,"It's third button" , "down" , "hover");
const  tooltip4 = new Tooltip("#btn-4" ,"It's fourth button" , "button" , "click");

// tooltip.render();
// tooltip.attachEvent();

const tooltiplist = [tooltip1 , tooltip2 , tooltip3 , tooltip4];

tooltiplist.forEach((tooltip) => {
    tooltip.init();
})