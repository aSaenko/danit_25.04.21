
let arrayList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function fnCreateList(arr,position) {

    let element = arr.map((item) => {
        //let elArr = `${"<li>" + item +"</li>"}`;
        let elArr = `<li>${item}</li>`;
        return elArr
    });

    let joinArr = element.join("");

    position.insertAdjacentHTML("afterbegin",joinArr);
}

fnCreateList(arrayList,document.body);