/**
 * Вибрати вказані елементи зі сторінки та вивесті їх в консоль
 *
 */
// -----
/**
 * всі блоки (div) на сторінці
 */
//let collectionDiv = document.getElementsByTagName("div");
//console.log(collectionDiv);
/**
 * всі кнопки на сторінці
 */
// let collectionBut = document.getElementsByTagName("button");
// console.log(collectionBut);
/**
 * Кнопку Add
 */
// let a = document.getElementById("add-button");
// console.log(a);
/**
 * список покупок
 */
// let shopping = document.getElementsByClassName("shopping-list")[0];
// console.log(shopping.innerText.split("\n"));// innerText - получили сам текстс обьекта
/**
 * список магазинів
 */
// let mag = document.getElementsByClassName("groceries")[0]; // без [0] не найдет
// console.log(mag.innerText);
/**
 * елементи списку покупок
 */
 //let shop = document.querySelectorAll(".shopping-list li.fruits"); // .fruits дает возможность еще подробнее копнуть
 //console.log(shop);
/**
 * фрукти в списку покупок
 */

/**
 * перший елемент в списку покупок
 */
//let shop = document.querySelector(".shopping-list");
//console.log(shop.children[0]); // так выбрал первый элемент с всех
/**
 * кнопки Cancel та Ok
 */
// let but = document.getElementById("control-buttons");
// console.log(but);

// let but = document.getElementById("control-buttons"); или так
// console.log(but.getElementsByTagName("button"));
/**
 * html код списку магазинів
 */
// let mag = document.getElementsByClassName("groceries")[0];
// console.log(mag.outerHTML);
/**
 * текст та колір першого елементу списку покупок
 */
//let col = document.querySelectorAll(".shopping-list li")[0];
//console.log(col.getAttribute("style")); // так получили
//console.log(col.style.color , col.innerText);  // или так

/**
 * текст та колір третього елементу списку покупок
 */
// let col = document.querySelectorAll(".shopping-list li")[2];
// console.log(col.style.color , col.innerText);