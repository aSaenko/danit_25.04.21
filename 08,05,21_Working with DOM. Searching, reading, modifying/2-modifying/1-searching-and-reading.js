const wrapper = document.getElementById('wrapper');
console.log(wrapper);
console.log(wrapper.style.borderColor);
console.log(wrapper.innerHTML);
console.log(wrapper.outerHTML);
console.log(wrapper.innerText);

const paragraphs = document.getElementsByClassName('paragraph');
console.log(paragraphs);
console.log(paragraphs[paragraphs.length - 1]);

const divs = document.getElementsByTagName('div');
console.log(divs);

const firstParagraph = document.querySelector('#wrapper .paragraph');
console.log(firstParagraph);

const allParagraphs = document.querySelectorAll('#wrapper .paragraph:nth-child(2)');
console.log(allParagraphs);
