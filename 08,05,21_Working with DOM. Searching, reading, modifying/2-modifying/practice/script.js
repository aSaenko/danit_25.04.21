/**
 * змінити текст кнопки Add на Add product
 */
// let btn = document.getElementById("add-button");
// btn.innerText = "Add product";
// console.log(btn.innerText);
/**
 * змінити перший елемент в списку покупок з
 * Milk на Apple
 */
// let item = document.querySelector(".shopping-list li");
// item.innerText = "apple";
/**
 * Зробити перший елемент в списку магазинів
 * червоним
 */
// let list = document.querySelector('.groceries li');
// list.style.color = "red";
/**
 * Сховати кнопку Cancel
 */
// let btn = document.getElementById("control-buttons").firstElementChild;
// btn.style.display = "none";
/**
 * зробити списку магазинів червону рамку
 * товщиною 2px
 */
// let list = document.querySelector(".groceries");
// list.style.border = "2px solid red";
/**
 * Змінити другий елемент в списку магазинів з
 * Novus на Auchan
 */
/*
let list = document
    .getElementsByClassName("groceries")[0]  // возвращается колекция и берется первый элемент
    .getElementsByTagName("li")[1] // в нутри мы ищем все li берем 2й элемент
    .innerText = "Auchan"; // и в него переделываем

 */
/**
 * додати першому елементу списку покупок клас fruits
 */
let list = document.getElementsByClassName("shopping-list")[0].firstElementChild;
list.classList.add("fruits");  // так можно добавить клас
//list.className = `${list.className} fruits`; // или так

/**
 * У блок fruits додати 2 елемента: заголовок 2го рівня та параграф
 * з довільними текстами
 *
 * innerHTML
 */
let div = document.querySelector("div.fruits");
div.innerHTML = '<h2>Header</h2> <p>Silpo</p>';
/**
 * Усім елементам в списку магазинів зробити колір зелений
 * (рішення повинно включати список з будь-якою к-тю магазинів)
 */

/**
 * Кожному елементу списку покупок додати клас shopping-list--item
 */

/**
 * Перевірити, чи має третій елемент списку покупок клас fruits
 */

/**
 * Елемент списку з текстом Coffee виділити коричневим кольором
 */
