const divWrapper = document.getElementById('wrapper');
console.log(divWrapper);
console.log(divWrapper.style.padding);
console.log(divWrapper.style.border);

divWrapper.style.padding = "40px";  // присвоение новых значений!
console.log(divWrapper.style.padding);

divWrapper.style.borderColor = "blue";

const firstParagraphElInWrapper = document.querySelector('#wrapper .paragraph');
const secondParagraphElInWrapper = document.querySelector('#wrapper .paragraph:nth-child(2)');
const thirdParagraphElInWrapper = document.querySelector('#wrapper .paragraph:nth-child(3)');
console.log(thirdParagraphElInWrapper);

thirdParagraphElInWrapper.innerText = 'Test';  // присвоение нового текста

secondParagraphElInWrapper.innerHTML = '<span class="italic">Second Element</span>'; // добавили полностью новую вложеность , так динамически можно что то добавлять в новую строку

console.log(firstParagraphElInWrapper.classList); // classList - манипулирует тем что у нас в нутри
firstParagraphElInWrapper.classList = 'not-active';