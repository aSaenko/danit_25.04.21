

let button = document.querySelector(".btn");
button.addEventListener("click" , function (event){
    event.preventDefault();
    let elInput = document.getElementById("amount");
    let elCurencyFrom = document.querySelector("#currency-1");
    let elCurencyTo = document.querySelector("#currency-2");
    let elResult = document.querySelector(".result");

    if (!elInput || !elCurencyFrom || !elCurencyTo) return;
    elInput = elInput.value;
    elCurencyFrom = elCurencyFrom.value;
    elCurencyTo = elCurencyTo.value;


    if(elCurencyFrom === elCurencyTo){
        elResult.querySelector(".given-amount").textContent = elInput;
        elResult.querySelector(".base-currency").textContent = elCurencyTo;
        elResult.querySelector(".second-currency").textContent = elCurencyTo;
        elResult.querySelector(".final-result").textContent = elInput;
        return;
    }

    fetch(" https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")
        .then((response)=> response.json())
        .then((curencyList)=> {

            const convertedMoney =  exchange(elInput,elCurencyFrom,elCurencyTo,curencyList);

            elResult.querySelector(".given-amount").textContent = elInput;
            elResult.querySelector(".base-currency").textContent = elCurencyTo;
            elResult.querySelector(".second-currency").textContent = elCurencyTo;
            elResult.querySelector(".final-result").textContent = convertedMoney.toFixed(2);

        })
        .catch((err) => console.log(err));
});

function exchange (money,curencyFrom,curencyTo,curencyList){
    const curensyFromProps = curencyList.find(({ ccy})=>{
        return ccy.toUpperCase() === curencyFrom;
    }) || {};
    const curensyToProps = curencyList.find(({ ccy})=>{
        return ccy.toUpperCase() === curencyTo;
    }) || {};

    if (curensyFromProps.base_ccy === curensyToProps.base_ccy){
        let convertMoney = money * curensyFromProps.buy;
        console.log(convertMoney);
        convertMoney = convertMoney / curensyFromProps.sale;
        console.log(convertMoney);
        return convertMoney;
    }else if (
        curensyFromProps.base_ccy === curencyTo.toUpperCase() ||
        curensyToProps.base_ccy.toUpperCase() === curencyFrom.toUpperCase()
    ){
        return  money * curensyFromProps.base_ccy === curencyTo.toUpperCase()
        ? curensyToProps.buy
            : curensyToProps.buy;
    }else {throw new Error("second currency not found")}
}