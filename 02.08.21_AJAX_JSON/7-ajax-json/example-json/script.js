const person = {
    name: 'John',
    age: 24,
    lang: ['php', 'js'],
    test: true,
}

const personStr = JSON.stringify(person);  // преобразует в строку
const personObj = JSON.parse(personStr)   // преобразует в обьект

console.log(person);
console.log(personStr);
console.log(personObj);






// это пред история про промисы
class Ponchik {}
class Belyash {}
class Shaurma {}

function facrotyFood (type) {
    switch (type) {
        case 1:
            return new Ponchik();
        case 2:
            return new Belyash();
        case 3:
            return new Shaurma();
    }

}

Promise.then(
    (success)=> console.log(success),
    (err)=> console.log(err),
)
    .then()
    .then()
    .catch((err) => {
        if (err.message === "Опаньк") return new Promise();
    })
