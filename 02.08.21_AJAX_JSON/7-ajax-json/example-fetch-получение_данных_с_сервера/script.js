

const promise =  fetch("https://jsonplaceholder.typicode.com/posts");  // запрос , возвращает промис , стяни, скачай данные

promise
    .then((resp) => {
        return resp.json(); // разобьет на обьекты
    })
    .then((data) => console.log("data :" , data[0].title))  // выведет первый элемент
    .catch(err => console.warn(err))   // скажи что сломалось
    .finally(() => console.log("Job done!!"));// напиши в конце




  // .then((response) => response.json())
  // .then((data) => {
  //   data.forEach((post) => {
  //     document.body.insertAdjacentHTML("beforeend", `<p>${post.title}</p>`);
  //   });
  // });
