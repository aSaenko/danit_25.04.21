/**
 * Доповнити функцію authenticate, яка буде резолвити успішно
 * проміс, якщо передали username: 'admin', password: '123', та
 * повертати помилку 'Invalid data' в іншому випадку
 *
 */

const authenticate = (username, password) => {
  return new Promise((resolve, reject) => {
    // Дописати код нижче
    setTimeout(() => {
      if (username === "admin" && password === "123") {
        resolve();
      } else {
        reject(new Error("Invalid data"));
      }
    }, 2000);
  });
};

authenticate("admin", "1234")
  .then(() => console.log("Success"))
  .catch((err) => console.error(err.message, "\nAccess denied"));
