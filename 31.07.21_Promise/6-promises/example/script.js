const promise = new Promise(function (resolve, reject) {
  setTimeout(() => {
    // resolve("Kyiv");
    reject(new Error("error"));
  }, 3000);
});

// promise
//   .then(
//     (result) => console.log(result)
//     // (err) => console.error(err)
//   )
//   .catch((err) => console.log("from catch", err))
//   .finally(() => console.log("DONE"));



function makeOrder() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("Order in progress");
      resolve();
    }, 500);
  });
}

function createMakeline() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("Makeline");
      resolve();
    }, 300);
  });
}

function inOven() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("In oven");
      if (Date.now() % 2) {
        resolve();
      } else {
        reject();
      }
    }, 400);
  });
}

function inBox() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("In the box");
      resolve();
    }, 10);
  });
}

function onTheWay() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("On the way");
      resolve();
    }, 0);
  });
}

console.log("i want pizza!!!");

// makeOrder(
//     () => createMakeline(
//       () => inOven(
//         () => inBox(
//           () => onWay(
//             () => console.log("din don!")
//           )
//         )
//       )
//     )
// );

makeOrder()
  .then(() => createMakeline())
  .then(() => inOven())
  .then(() => inBox())
  .then(() => onTheWay())
  .then(() => console.log("din don !!!!!!"))
  .catch(() => console.error("сиди голодный!"))
  .finally(() => console.log("good job!"));
