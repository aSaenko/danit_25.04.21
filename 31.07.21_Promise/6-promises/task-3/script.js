/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */
let validCity = ["KYIV", "LVIV", "KHARKIV"];

function getWeather(city) {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (validCity.includes(city.toUpperCase())) {
        resolve();
      } else {
        reject(new Error("Невідоме місто"));
      }
    }, 1000);
  });
}

getWeather("kyiv")
  //   .then(() => console.log("good job"))
  .catch((error) => console.log(error.message));
