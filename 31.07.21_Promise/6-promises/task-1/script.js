/**
 * productsPromise робить запит на сервер і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки (alert)
 *
 */

const productsPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    if (Math.random() < 0.5) {
      resolve(["orange", "apple", "pineapple", "melon"]);
    } else {
      reject(new Error("Server Error"));
    }
  }, 2000);
});

productsPromise
  .then((data) => {
    const html = data.reduce(function (acc, item) {
      acc += `<li>${item}</li>`;
      return acc;
    }, "");
    const container = document.querySelector(".loader");
    container.textContent = "";
    container.insertAdjacentHTML("afterbegin", `<ul>${html}</ul>`);
  })
  .catch((error) => alert(error.message));
