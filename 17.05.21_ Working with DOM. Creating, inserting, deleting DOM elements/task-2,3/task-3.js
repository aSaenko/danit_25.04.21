
// дати користовачу створити квадрати на сторинци

// користувач спочатку вводить 50 потим red 1 на стор1нц1 зьявляеться квадрат розм1ром 50 пкс

const squareBuilder = (width, bgcolor) => {
    let square = document.createElement("div");
    square.style.width = `${width}px`;
    square.style.height = `${width}px`;

    square.style.backgroundColor = bgcolor;
    return square
};

let userRequest = prompt("input width and color");

let userSquareParams = userRequest.split(",");  // так мы ищем чем будет юзер разделять

let squareElement = squareBuilder(userSquareParams[0].trim(),userSquareParams[1]); // тримом убрали пробелы

document.body.append(squareElement); // этим мы вставляем в нашу HTML размеку что бы на в браузере было видно результат

