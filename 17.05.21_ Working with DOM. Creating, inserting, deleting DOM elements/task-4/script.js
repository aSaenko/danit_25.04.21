// в1добразити заданий автомоб1л1в у вигляд1 НАЗВА МОЖЕЛЬ
// eлектричн1 вид1лити зеленим а 1нш1 с1рим


const cars = [
    {
        name:"Tesla",
        model:"S",
        isElectric :true,
    },
    {
        name:"Nisan",
        model:"Leaf",
        isElectric :true,
    },
    {
        name:"BMW",
        model:"X3",
        isElectric :false,
    },
    {
        name:"BMW",
        model:"i3",
        isElectric :true,
    },
    {
        name:"Toyota",
        model:"Camry",
        isElectric :false,
    },
];

const carElementBuilder = (name,model, isElectric) => {  // эта функция будет штамповать и делить машины на цвета

    let element = document.createElement("li");
    element.innerText = `${name.toUpperCase()} ${model.toUpperCase()}`;
    element.style.color = isElectric ? "green" : "grey";
    return element
};

const carListEl = cars.reduce((acc, car)=>{
    let elCar = carElementBuilder(car.name,car.model,car.isElectric);
    acc.append(elCar);
    return acc;
}, document.createElement("ul"));

document.body.append(carListEl);
