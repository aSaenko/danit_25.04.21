// DOMContentLoaded  -- выполнится только тогда когда прогрузится контент страници

window.addEventListener('DOMContentLoaded', function () {

    window.addEventListener('mousemove', function (event) {
        // console.log(event);
    })
});

const buttonEl = document.querySelector("#btn");

// mousedown + moumouseup = click => так показано в какой последвательености будет работать клик
// сделать что бы при на жатии на элемент кликало
let count = 0;
buttonEl.addEventListener("click", function () { // click здесь как метод, типа что должен сделать
  count ++;   // такой фигней сделали счетчик и в консоль будет выводить сколько раз нажали
  console.log("clicked 123123", count);
});


// этим кодом делаем так что будет следить когда заходит и выходит мышка за поля!!
 const blockEl = document.querySelector('#block');

 const mouseenterFn = function (event) {  // если мышка в пределы поля
     console.log('moved over');
     console.log('');
 };

blockEl.addEventListener("mouseenter",mouseenterFn);

 blockEl.addEventListener('mouseleave', function (event) {  // если мышка выйдет за пределы поля
     console.log('moved out');
     console.log('');

     blockEl.removeEventListener("click",mouseenterFn); // так удалить слушателя событий
 });



// const headingEl = document.querySelector('h1');
// headingEl.addEventListener('mouseenter', function (event) {
//     // blockEl.style.display = 'block';
//     blockEl.style.backgroundColor = 'red';
// })

// headingEl.addEventListener('mouseleave', function (event) {
//     // blockEl.style.display = 'none';
//     blockEl.style.backgroundColor = 'green';
// })

// blockEl.addEventListener('click', function (event) {
//     console.log(this);
//     console.log(event.target);
//     this.style.width = '600px';
// })

// console.log(buttonEl);
