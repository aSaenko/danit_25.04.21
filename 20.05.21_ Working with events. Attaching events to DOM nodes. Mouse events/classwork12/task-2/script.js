/**
 * При натисканні на кнопку Add додавати
 * квадрат в блок squares-container
 * Для квадрату стилі вже є наявні
 *
 * ADVANCED: при додаванні задавати колір квадрату випадковим чином
 *
 */
const mathRandomFn =(maxValue)=>{Math.trunc(Math.random()*maxValue)}; // функция генерирует от 0 , через аргумент можем задать любое свое макс значение
const setRandomColor = (element)=>{
    const randomColor = `hsl(${mathRandomFn(360)},100%,50%)`; // выбор самого цвета и в каком формате
    element.style.backgroundColor = randomColor;
};


const appendSquare = () => {
    let elDivSquare = document.createElement("div");
    elDivSquare.classList.add("square");  // диву добавляем калсс
   // let elContainer = document.getElementsByClassName("squares-container")[0];  // одно и то же что в низу если надо найти первый элемент
    let elContainer = document.querySelector(".squares-container"); // ищем элементы дива которые будут добавлятся
    setRandomColor(elDivSquare); // этим добавили нашу функцию с рандомным цветом
    elContainer.append(elDivSquare);  // будет добавлять созданые элементы в окнец
};

const elAddButton = document.getElementById("add-btn");

elAddButton.addEventListener("click",appendSquare);