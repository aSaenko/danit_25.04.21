/**
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 *
 * ADVANCED: Додати можливість видаляти елементи зі списку покупок
 * (додати Х в кінець кожного елементу, при кліку на який відбувається
 * видалення) та відмічати як уже куплені (елемент перекреслюється при
 * кліку на нього)
 * Для реалізації цього потрібно розібратися з Event delegation
 * https://javascript.info/event-delegation
 */

/*
const appendNewItem = (text) => {              // такой функцией создается и добавляются эулументы один за дургим
    const elNewItemList = document.createElement("li");
    elNewItemList.innerText = text;
    const elContainer = document.querySelector(".shopping-list");
    elContainer.append(elNewItemList);
};


const addHandler = () => {   // этой функцией мы принимаем и передаем ее на складывания введеный текс
    const elInput = document.querySelector("#new-good-input");
    const value = elInput.value;

    if (!value)return;  // что бы не добавлялось лишнее, то есть если введут пустое поле то ничего не добавится если убрать то будут просто точки

    appendNewItem(value);
    elInput.value = "";  // для того чтобы после ввода оставалось пустое поле
};

const elAddButton = document.getElementById("add-btn");  //этим добаляем событие клика
elAddButton.addEventListener("click",addHandler);
 */

//ADVANCED


const elContainer = document.querySelector(".shopping-list");

const appendNewItem = (text) => {              // такой функцией создается и добавляются эулументы один за дургим
    const elNewItemList = document.createElement("li");
    elNewItemList.innerText = text;  // пишем что у нас принимается текст
    const elContainer = document.querySelector(".shopping-list");

    const elNewBtn = document.createElement("button"); // для крестика
    elNewBtn.innerText = "delete";  // тут на прямую написали что будет в тексте
    elNewBtn.className = "js-remove-item";

    elNewItemList.append(elNewBtn);

    elContainer.append(elNewItemList);




};

const addHandler = () => {   // этой функцией мы принимаем и передаем ее на складывания введеный текс
    const elInput = document.querySelector("#new-good-input");
    const value = elInput.value;

    if (!value)return;  // что бы не добавлялось лишнее если убрать то будут просто точки

    appendNewItem(value);
    elInput.value = "";
};

const elAddButton = document.getElementById("add-btn");  //этим добаляем событие клика
elAddButton.addEventListener("click",addHandler);


const listHandler = event => {    // таким образом мы находим то на что нажимаем!!!
    console.log(event.target);
    //event.target.remove();   // если оставлю только это то удалит все элементы!
    if (event.target.className.includes("js-remove-item")) {   //этим всем мы пишем что бы при удалении удаляло только по одному
        const parent = event.target.parentElement;
        parent.remove()
    }
};
elContainer.addEventListener("click",listHandler);