/**
 * При натисканні на кнопку Login показувати модальне
 * вікно з текстом Success login
 *
 */

const elLoginButton = document.getElementById("login-btn");
// elLoginButton.addEventListener("click",() =>{    - такой вариант
//     alert("Success login");
//});


//elLoginButton.onclick =(event) => {     //тиким образом можем получить много информации про кнопку
//    console.log(event);
//};

elLoginButton.onclick =(event) => {
    if (event.ctrlKey){              // если нажимать с кнтрл то будет работать если без будет фолс
        alert("Success login");
    } else {
        alert("Go away!!")
    }
};