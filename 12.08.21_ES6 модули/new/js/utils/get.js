
function get(url,queryParams){
    let queryParamsString = new URLSearchParams
    return fetch(`${url}?${queryParamsString}`).then((resp) => {
        if (resp.ok){
            return resp.json()
        }else {
            throw new Error(resp.status)
        }
    })
}
export default get;