import get from "../utils/get.js";

export const BASE_URL = "https://picsum.photo/v2/list";
export const PHOTOLIMITS = 9

export async function getPhoto(page = 1){
    try {
        await get(BASE_URL + "/list",{page , limit : PHOTOLIMITS})

    }catch (e){
        throw new Error(e);
    }
}

export async function hasPhotosNextPage(currentPage){
    const photos = await get(BASE_URL + "/list",{page: currentPage+1 , limit: PHOTOLIMITS});
    return Array.isArray(photos)&& photos.length>0


}

// export default getPhotos;