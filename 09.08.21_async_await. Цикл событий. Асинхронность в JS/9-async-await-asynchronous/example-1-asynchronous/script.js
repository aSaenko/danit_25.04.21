function fetchFilms (){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            resolve([{id: 1, user: "Ivan" , admin : false}]);
        },2000)
    });
}

async function test(){  // тут указывает интерпритатору что будем работать асинхронно
    try {
        const films = await fetchFilms();
        const actors = [];

        // films.forEach(async(film) =>{
        //     const actors = await fetchFilms(film.name)
        // });

        for (let key in films){
            const actor = await fetchFilms(films[key].name);
            actors.push(actor);
        }

    }catch (e){
        console.error(new Error("Opanki"))
    }finally {
        console.log("WORK DONE")
    }

    console.log({data});
}
test();

// const asyncfn = async ()=> {    // возвращает промисы
//     return 1 ;
// }
// // // тоже само что и это
// // const  asyncFn = new Promise((resolve)=>{
// //     resolve(1);
// // })
// console.log(asyncfn());