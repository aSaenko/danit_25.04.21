


class Post {  // показываем что должен делать
    baseUrl = "https://ajax.test-danit.com/api/json"

    constructor(id, postID, commentsId) {
        this.postID = postID;
        this.commentsId = commentsId;
        this.id = id;
    }

    async fetchPost() {   //собирате посты
        const url = this.baseUrl + "/posts/" + this.id   // пост что бы конкретно чтот пост выбирать
        try {
            const response = await fetch(url)  //отклик
            const post = await response.json()  //возвращает промис
            this.post = post;
        } catch (e) {
            console.error(e);
        }
    }
    async fetchComments(){  // собирает коменты
        const url = this.baseUrl + "/posts/" + this.id + "/comments/";

        try {
            const response = await fetch(url)  //отклик
            const comments = await response.json()  //возвращает промис
            this.comments = comments;
        } catch (e) {
            console.error(e);
        }
    }

    renderPost(){ // привязка уже к странице поста
        const elPostContainer = document.getElementById(this.postID)
        const elPostTitle = elPostContainer.querySelector(".title");
        const elPostBody = elPostContainer.querySelector(".post-body");

        const {title , body} = this.post;  //деструктуризируем
        elPostTitle.textContent = title;
        elPostBody.textContent = body;
    }

    renderComments(){   // привязка уже к странице комента
        const elCommentsContainer = document.getElementById(this.commentsId);
        const elCommentsList = elCommentsContainer.querySelector(".comments-list");

        elCommentsList.innerHTML = "";
        this.comments.forEach(({body})=>{
            elCommentsList.insertAdjacentHTML("afterbegin" , `<li>${body}</li>`);

        })
    }

}

const post = new Post(null ,"card" , "comments");
const postSelector = document.getElementById("post");  // каждый раз когда ты меняешься подставляй эти значения

postSelector.addEventListener("change" , async (event)=>{
    post.id = postSelector.value;

    await post.fetchPost();   // скай все данные
    await post.fetchComments();

    post.renderPost();  // и отрендери
    post.renderComments();
})