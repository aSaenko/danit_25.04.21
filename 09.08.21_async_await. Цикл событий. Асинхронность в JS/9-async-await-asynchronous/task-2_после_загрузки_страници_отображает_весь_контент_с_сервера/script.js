
// // мой КОД , НЕ РАБОТАЕТ , но с коментами
// const baseUrl = "https://ajax.test-danit.com/api/json"
//
// // api которая булет по ключу возвращать url
//
// const USER_KEY = "USER";
// const TASKS_KEY = "TASKS"
// const ROUTE_BY_NAME = new Map([
//
//         [USER_KEY,() => baseUrl + "/users/"],
//         [TASKS_KEY,(id) => baseUrl + "/users/" + id + "/todos"],
//
// ]);
//
// // метод который ходит и полуает юзеров
// async function fetchUser(){
//     const getUrl = ROUTE_BY_NAME.get(USER_KEY);
//
//     const resp = await fetch(getUrl())
//     const data = await resp.json();
//
//     return data;
// }
//
// async function fetchToDos(id) {
//     const getUrl = ROUTE_BY_NAME.get(TASKS_KEY);
//
//     const resp = await fetch(getUrl(id))
//     const data = await resp.json();
//
//     return data;
// }
// function renderUser(container, email, name, userName, companyName){  // ставим в разметку
//
//     const html = `<div class="card user-card">
//     <div class="card-header">
//         <a href="mailto:${email}">${name}</a>
//         <span>(${userName})</span>
//       </div>
//   <div class="card-body">
//       <ul class="list-group task-list"></ul>
//   </div>
//   <div class="card-footer">
//     <small class="text-muted">${companyName}</small>
//   </div>
// </div>`;
//
//     //   const container = document.querySelector(".tasks-board");
//
//     container.insertAdjacentHTML("beforeend", html);
// }
//
// function renderToDos(container,title ,isComplete) {
//     const html = `<li class="list-group-item task-list--item list-group-item-action
// ${isComplete ? "-completed disable" : ""}">${title}</li>`
// const elList = container.querySelector(".tasks-list");
//     elList.insertAdjacentHTML("beforeend",html);
// }
//
// document.addEventListener("DOMContentLoaded" ,async (event) => { // сразу после загрузки страници
//     const users = await fetchUser(); // массив
//     const container = document.querySelector(".tasks-board");
//     for (let i = 0; i < users.length ; i++){
//         const {
//             id,
//             email ,
//             username,
//             company:{name: companyName}
//         } = users[i];
//         renderUser(container ,email, name, username , companyName)
//
//         const userContainer = container.querySelectorAll(".card.user-card")[i];
//
//         const toDos = await fetchToDos(id);
//
//         toDos.forEach(({ title,complete }) => {
//             renderToDos(userContainer, title ,complete )
//         });
//     }
// });



// рабочий код


const baseUrl = "https://ajax.test-danit.com/api/json";
const USER_KEY = "USER";
const TASKS_KEY = "TASKS";
const ROUTE_BY_NAME = new Map([
    [USER_KEY, () => baseUrl + "/users"],
    [TASKS_KEY, (id) => baseUrl + "/users/" + id + "/todos"],
]);

async function fetchUsers() {
    const getUrl = ROUTE_BY_NAME.get(USER_KEY);

    const resp = await fetch(getUrl());
    const data = await resp.json();

    return data;
}

async function fetchToDos(id) {
    const getUrl = ROUTE_BY_NAME.get(TASKS_KEY);

    const resp = await fetch(getUrl(id));
    const data = await resp.json();

    return data;
}

function renderUser(container, email, name, userName, companyName) {
    const html = `<div class="card user-card">
    <div class="card-header">
        <a href="mailto:${email}">${name}</a>
        <span>(${userName})</span>
      </div>
  <div class="card-body">
      <ul class="list-group task-list"></ul>
  </div>
  <div class="card-footer">
    <small class="text-muted">${companyName}</small>
  </div>
</div>`;

    //   const container = document.querySelector(".tasks-board");

    container.insertAdjacentHTML("beforeend", html);
}

function renderToDos(container, title, isComplete) {
    const html = `<li class="list-group-item task-list--item list-group-item-action ${
        isComplete ? "-completed disabled" : ""
    }">${title}</li>`;
    const elList = container.querySelector(".task-list");
    elList.insertAdjacentHTML("beforeend", html);
}
document.addEventListener("DOMContentLoaded", async (event) => {
    const users = await fetchUsers();
    const container = document.querySelector(".tasks-board");
    for (let i = 0; i < users.length; i++) {
        const {
            id,
            email,
            username,
            name,
            company: { name: companyName },
        } = users[i];
        renderUser(container, email, name, username, companyName);

        const userContainer = container.querySelectorAll(".card.user-card")[i];

        const toDos = await fetchToDos(id);

        toDos.forEach(({ title, completed }) => {
            renderToDos(userContainer, title, completed);
        });
    }
});
