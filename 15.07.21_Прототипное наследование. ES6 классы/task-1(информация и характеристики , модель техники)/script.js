
// общее
class Device {
    constructor(brand , model ,price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    getName(){
        return `${this.brand} ${this.model} `
    }
    getPrice(){
        return this.price * 1.2
    }
}
//создаем отдельные классы  этим проводим наследование
class  Phone extends Device{
    constructor(brand , model ,price , nfc) {
        super(brand , model ,price);
        this.nfc = nfc;
    }
    getDeliveryPrice(){
        const price = this.getPrice();
        return price >= 10000 ? price * 0.01 : price *0.03;
    }
}

class  Tablet extends Device{
    constructor(brand , model ,price , sim) {
        super(brand , model ,price);
        this.sim = sim;
    }
    getDeliveryPrice(){
        const price = this.getPrice();
        return price >= 20000 ? 0 : price * 0.01;
    }
}

class  NoteBook extends Device{
    getDeliveryPrice(){    // якщо ціна з націнкою більше 30 000, то доставка безкоштовна
                               //* інакше доставка коштує 200
        return this.getPrice() >= 3000 ? 0 : 200;
    }
}

const noteBook1 = new NoteBook ("Huawei" , "Matebook 012" , "24999" )
const noteBook2 = new NoteBook ("Apple" , "MacBook Air 13" , "5144" )
const noteBook3 = new NoteBook ("Acer" , "Matebook 012" , "5144" )

const phone1 = new Phone("Apple ", "iphone12" ,"27999" , true );
const phone2 = new Phone("Samsung ", "Galaxy S21" ,"28999" , true );

const tablet = new Tablet("Lenovo ", "Yoga Tab" ,"7999" , false );

console.log(noteBook1.getName(),phone1.getName(),tablet.getName());


