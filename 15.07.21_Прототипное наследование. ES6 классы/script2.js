

// example with html Buttom
// buttn get id, text and type
// and render

class Person{
    group = "pe33";

    // static printGroup() {
    //     console.log(Person.group);
    // }

    constructor(firstName , lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    makeCoffee() {
        console.log("making coffee");
    }
}

class Student extends Person {   // наслодовались
    constructor(firstName , lastName) {
        super(firstName , lastName);  //вызывает функцию конструктор
    }

    attendLesson() {  // добавили новый метод
        console.log(`${this.firstName} attending lesson`)
    }
}

const person = new Person("Andrew" , "Petrow");
const student = new Student("Valerii" , "Matokin");

console.log(person);
console.log(student);
