

// const student = Object.create({});  // станет прототипом создает зависимость к обьекту который укажим к душках криейта
//
// student.attestation = function () {
//     console.log()
// }
// console.log(student);

// функция конструктор с прототипом

function Person (firstName, lastName){   // функция конструктор
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.makeCoffee = function (){
    console.log("making coffee");
}
function Student(firstName, lastName) {  // описываем функцияю она будет создавать новую ичейку памяти с динамическими значениями которые будет давать this

    Person.call(this.firstName, lastName);   // вызываем новый персон через колл
}

Student.prototype = Object.create(Person.prototype);

//создаем специальный метод для студента
Student.prototype.attendLesson = function (){
    console.log(`${this.firstName} attending lesson`);
};

const person = new Person("Andrew" , "Petrow");

const student = new Student("Valerii" , "Matokin");

console.log(person);
console.log(student);
