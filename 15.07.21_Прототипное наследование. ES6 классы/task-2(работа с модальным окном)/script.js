/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу redner() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */




class Modal {
    active = false;  // по дефолку модальное окно не активное

    constructor(id, text , classList) {
        this.id = id;
        this.text = text;
        this.classList = classList;
    }

    render() {
        const root  = document.getElementById(this.id)  // это окно которое создам то и найдет
        if (!root) return console.error("Исходный родительский элемент не найден")

        const elModal = document.createElement("div");
        elModal.innerHTML =
            `<div id="${this.id}-modal" class="modal ${this.classList}">
              <div class="modal-content">${this.text}
              <span class="close">X</span>
              </div>
              </div>`

        root.after(elModal)
    }

    open(){
        if (this.active) return;  // проверка на состояние

        const elModal = this.getModalElement();

        if (!elModal) {
            elModal.classList.remove("active")
            this.active = true;
        }
    }

    close(){
        if (!this.active) return;

        const elModal = this.getModalElement();

        if (!elModal) {
            elModal.classList.remove("active")
            this.active = false;
        }
    }

    getModalElement(){
        const elModal = document.getElementById(this.id+"-modal")

        if (!elModal) console.error("modal not mounted");

        return elModal;
    }
}

const modal = new Modal("login-btn" , "Hello" , "testClassList")
modal.render()



