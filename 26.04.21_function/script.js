


//умножение чисел с валидацией
/*
function validate(a) {
    const isValid = typeof a === "number";

    if (!isValid) console.error("Error type", typeof a);

    return isValid
}

function multi(a,b) {
    if (validate(a) && validate(b)){
        return a * b;
    }
}

console.log(multi(5,"test"));
console.log(multi(12,2));
console.log(multi(4,3));
 */


// написать Функцию которая проверяет число непарное
// и не делится на 5 вернуть false/true
/*

function testNumbers(a) {
    return a % 2 !== 0 && a % 5 !==0;
}
console.log(testNumbers(12));
console.log(testNumbers(7));
console.log(testNumbers(100));

 */

// факториал - (5 = 1 * 2*3*4*5 = 120 ) - факториал сначала идет в одну сторону потом в другую
/*
function factorial(n) {
    console.log("Get factorial from",n);
    if (n === 0 )return 1;

    return n * factorial(n-1)
}

const result = factorial(5);
console.log(result);
 */

// написать функцию для определения высокосный год или нет
// проверить 1980 , 2003 , 2020
// число року маэ одночасно дилиться на 4 та не дилиться на 100 або дилиться 400
/*
function leapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}
console.log(leapYear(1980));
console.log(leapYear(2003));
console.log(leapYear(2020));
 */


// функция для конвертации валют
// -1200  /  -2800  /  -3400
/*
const USD_UAH = 27.85;

function valuta(uah) {
    if (typeof  uah === "number" && uah >= 0 && isFinite(uah)){   // этим проводим валидацию на число

        return (uah / USD_UAH).toFixed(2);   // toFixed - сделвет ответ 2 цифры после комы
    } else {
       console.error("Incorect")
    }

}

console.log(valuta(1200));
console.log(valuta("jj"));
console.log(valuta(3400));
 */


// Написать функцию конвертации сум гривны в долары евро

const USD = 27.85;
const EUR = 31.55;
const USD_CURRENCY_NAME = "USD";
const EUR_CURRENCY_NAME = "EUR";

function exchangeUAH (uah, currency) {  // currency - валюта
    let currentCurrencyRate ;
    
    switch (currency) {
        case USD_CURRENCY_NAME:
            currentCurrencyRate = USD;
            break;

        case EUR_CURRENCY_NAME:
            currentCurrencyRate = EUR;
            break;

        default:
            currentCurrencyRate = 1;
    }
    const exchangedSum = (uah / currentCurrencyRate).toFixed(2);
    return `${exchangedSum}` ;
}

console.log(exchangeUAH(1000 , USD_CURRENCY_NAME));
console.log(exchangeUAH(154 , EUR_CURRENCY_NAME));
console.log(exchangeUAH(4000 , EUR_CURRENCY_NAME));



// сам.
/*
let USD = 27.85;

function valuta(uah) {
    if (typeof uah === "number" && uah > 0 ){

        return (uah / USD).toFixed(2)
    }

}
console.log(valuta(54452));
 */