

let inputBg = document.querySelector("#inpt");

inputBg.addEventListener("focus",() => {
    inputBg.classList.add("border-focus");
    falseText.remove()
},true);
inputBg.addEventListener("blur", () => inputBg.classList.remove("border-focus"),true);

const elContainer = document.querySelector(".words-price");

const appendNewItem = (text) => {
    const elNewItemList = document.createElement("span");
    elNewItemList.className = "span-price";
    elNewItemList.innerText = `${text}`;

    elContainer.append(elNewItemList);

    // const elDivContainer = document.querySelector(".price");
    // let elClone = elDivContainer.cloneNode(true);
    // console.log(elClone);
    // elClone.append(elClone);

    let newBtnX = document.createElement("button");
    newBtnX.innerText = "X";
    newBtnX.className = "button-x";
    if (text.length <=0)return;
    elNewItemList.append(newBtnX);
};

let falseText = document.createElement("p");
falseText.className = "error";
falseText.innerText = "Please enter correct price";

const addText = () => {
    let elInput = document.querySelector("#inpt");
    const value = elInput.value;

    if (value > 0){
        let containerDivTrue = document.querySelector(".price");
        containerDivTrue.style.backgroundColor = "#78d878";
        inputBg.classList.remove("border-focus-false");

    } else {
        elInput.value = "";
        inputBg.classList.add("border-focus-false");

        let addP = document.querySelector(".div-for-span");
        addP.append(falseText);

        document.querySelectorAll('.span-price').forEach(e => e.remove());

        return
    }

    appendNewItem(value);
    elInput.value = "";
};
inputBg.addEventListener("blur",addText);

const removeBtn = (event) =>{
    if (event.target.className.includes("button-x")){
        const parent = event.target.parentElement;
        parent.remove();

        let containerDivFalse = document.querySelector(".price");
        containerDivFalse.style.backgroundColor = "transparent";
    }
};
elContainer.addEventListener("click", removeBtn);

