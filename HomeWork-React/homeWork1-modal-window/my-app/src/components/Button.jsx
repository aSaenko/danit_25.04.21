import React from "react";
import "../style/btn.scss"
class Button extends React.Component{
    render() {

        return(
            <div className="wrapper-button">
                <button id={this.props.id} onClick={this.props.onClick} className="button__first">
                    {this.props.nameBtn}
                </button>
            </div>
        )
    }
}

export default Button