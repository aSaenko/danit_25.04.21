import React from "react";
import "../style/modalStyle.scss"


class Modal extends React.Component {
    render() {
        return (
            <div className={`container-modal ${this.props.isOpen ? "open" : "close"}`} onClick={this.props.zone}>
                <div className={this.props.styleModal} onClick={e => e.stopPropagation()}>
                    <div className="modal__header">
                        <h1 className="modal__question">{this.props.textHeader}</h1>
                        <div className="close" onClick={this.props.modalClose}> </div>
                    </div>
                    <div className="modal__wrapper-button">
                        <p className="modal__btnText">{this.props.btnText}</p>
                        <div className="modal__wrapper-btn">
                            <button className={this.props.styleBtnOk}>Ok</button>
                            <button className={this.props.styleBtnCancel} onClick={this.props.modalClose}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal