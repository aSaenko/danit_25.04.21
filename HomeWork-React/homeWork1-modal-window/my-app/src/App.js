import React from "react";
// import logo from './logo.svg';
// import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";


class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {showModal: false}
        this.state = {modal1 : false}
        this.state = {modal2 : false}

    }
    btnClickHandler = ()=> {
        this.setState({showModal: !this.state.showModal})
    }
    btnClickModal1 = ()=> {
        this.setState({modal1: !this.state.modal1})
    }
    btnClickModal2 = ()=> {
        this.setState({modal2: !this.state.modal2})
    }

  render() {

        let modal1 = {
            textHeader: "Do you want to delete this file?",
            btnText: "Once you delete this file, it won’t be possible to undo this action.\n" +
                "                            Are you sure you want to delete it?",
        }

        let modal2 = {
            textHeader: "Вы хотите удалить этот файл?",
            btnText: "После удаления этого файла это действие будет невозможно отменить. " +
                "Вы уверены, что хотите удалить его?",
        }

    return (
        <React.Fragment>

          {/*<BtnAddCard id="1" onClick={this.btnClickModal1} nameBtn="Open first modal"/>*/}
          {/*<BtnAddCard id="2" onClick={this.btnClickModal2} nameBtn="Open second modal"/>*/}

            {/*{this.state.modal1 &&*/}
            {/*    <Modal*/}
            {/*    textHeader={modal1.textHeader}*/}
            {/*    btnText={modal1.btnText}*/}
            {/*    btnClickModal1={this.state.btnClickModal1}/>}*/}

          {/*  {this.state.modal2 &&*/}
          {/*  <Modal*/}
          {/*      textHeader={modal2.textHeader}*/}
          {/*      btnText={modal2.btnText}*/}
          {/*      btnClickModal2={this.state.btnClickModal2}/>}*/}

<div className="wrap">
            <Button onClick={this.btnClickModal1} nameBtn="Open first modal"/>
            <Button onClick={this.btnClickModal2} nameBtn="Open second modal"/>
</div>
            <Modal
                zone={this.btnClickModal1}
                isOpen = {this.state.modal1}
                textHeader={modal1.textHeader}
                btnText={modal1.btnText}
                modalClose={this.btnClickModal1}
                styleModal={"modal__first"}
                styleBtnOk={"modal__btn"}
                styleBtnCancel={"modal__btn"}
            />

            <Modal
                zone={this.btnClickModal2}
                isOpen = {this.state.modal2}
                textHeader={modal2.textHeader}
                btnText={modal2.btnText}
                modalClose={this.btnClickModal2}
                styleModal={"modal__second"}
                styleBtnOk={"modal__btn2"}
                styleBtnCancel={"modal__btn2 cancel"}
            />
        </React.Fragment>
    );
  }
}

export default App;
