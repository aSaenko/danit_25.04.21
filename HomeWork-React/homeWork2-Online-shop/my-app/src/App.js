import React from "react";
import getProduct from "./fixtures/product";
import ProductList from "./components/ProductList";
import "./style/style.scss"
import Header from "./components/Header";



class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {item :[]};

    }
    // попробывать сохранять в локал значения избранного
    componentDidMount() {
        getProduct()
            .then((data) => {
                this.setState({ item: data });
            })
    }

    render() {
        return (
            <>
            <Header/>
            <div className="wrapper-product-card">
                <ProductList
                    products={this.state.item}
                />
            </div>
            </>
        );
    }
}

export default App;
