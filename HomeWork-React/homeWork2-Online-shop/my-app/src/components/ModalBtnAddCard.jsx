import React from "react";

class Modal extends React.Component {
    render() {
        return (

            <div className={`container-modal ${this.props.isOpen ? "open" : "close"}`} onClick={this.props.zone}>
                <div className={"modal__container"} onClick={e => e.stopPropagation()}>
                    <div className="close" onClick={this.props.modalClose}> </div>
                        <div className="modal__header">
                             <h1 className="modal__question">Добавить в Корзину?</h1>
                        </div>
                    <div className="modal__wrapper-button">
                            <p className="modal__btnText">{this.props.btnText}</p>
                        <div className="modal__wrapper-btn">
                            <button className="modal__ok-cancel" >Добавить</button>
                            <button className="modal__ok-cancel" onClick={this.props.modalClose}>Отмена</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Modal