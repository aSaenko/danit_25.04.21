import React from "react";

class BtnAddCard extends React.Component{
    render() {

        return(
            <div className="wrapper-button">
                <button onClick={this.props.onClick} className="product-card__add-card">
                    {this.props.nameBtn}
                </button>
            </div>
        )
    }
}

export default BtnAddCard