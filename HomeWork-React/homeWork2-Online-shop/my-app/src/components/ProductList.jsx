import React from "react";
import ProductBlock from "./ProductBlock";

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            favoriteIn: false,
        }
        this.addToFavorite = this.addToFavorite.bind(this);  // без этого функция не работает
    }
    //
    // addToFavorite(e){
    //     const {id} = e.target
    //     const updateItem = this.props.products.map((item)=>{
    //         if (id === item.id){
    //                 if (this.state.favoriteIn === false){
    //                     this.state.favoriteIn = true
    //                     localStorage.setItem(`fav + ${id}`, this.state.favoriteIn)
    //                 }else {
    //                     item.favoriteIn = false;
    //                     localStorage.setItem(`fav + ${id}`, this.state.favoriteIn)
    //                 }
    //         }
    //     })
    //     this.setState({item: updateItem})
    // }
// saveLocal(){
//     localStorage.getItem('fav')
// }
        addToFavorite(e){
        const {id} = e.target
        const updateItem = this.props.products.map((item)=>{
            if (id === item.id){
                    if (item.favorite === false){
                        item.favorite = true
                        localStorage.setItem(`${id}`, item.favorite)
                    }else {
                        item.favorite = false;
                        localStorage.setItem(`${id}`, item.favorite)
                    }
            }
        })
        this.setState({item: updateItem})
    }
    getStateFavorite(){
        // этой функцией получать / считывать с локалстора состояние избрвнного
    }

    render(){
        return(
            <>
                {this.props.products.map((item) => {
                    return(
                        <ProductBlock
                            name={item.name}
                            price={item.price}
                            img={item.img}
                            id={item.id}
                            key={item.id}
                            color={item.color}
                            inFavorites = {item.favorite}
                            onClick={this.addToFavorite}
                            products={item}
                        />)
                    }
                )}
            </>
        )
    }
}

export default ProductList