import React from "react";
import BtnFavorite from "./BtnFavorite";
import BtnAddCard from "./BtnAddCard";
import ModalBtnAddCard from "./ModalBtnAddCard";

class ProductBlock extends React.Component{
    constructor(props) {
        super(props);
        this.state = {showModal: false}
    }

    btnClickHandler = ()=> {
        this.setState({showModal: !this.state.showModal})
    }

    setHidden = () => {
    const modalContainerOpen = document.querySelectorAll(".container-modal.open")
        if (modalContainerOpen) {
            document.body.style.overflow = "hidden";
        }
    }
    setScroll = () => {
        document.body.style.overflow = "scroll";
    }

    render() {
        return(
            <div className="product-card" id={this.props.id}>
                <div className="product-card__content">
                    <img src={this.props.img} alt="" className="product-card__img"/>
                    <h2 className="product-card__name" >{this.props.name}</h2>
                    <p typeof="number" className="product-card__price" style={{color:this.props.color}}>{this.props.price}₴</p>
                    <div className="btn-container">
                        <BtnAddCard
                            nameBtn={"Add to cart"}
                            onClick={()=>{
                                this.btnClickHandler()
                                this.setHidden()
                            }}
                        />
                        <BtnFavorite
                            id={this.props.products.id}
                            inFavorites = {this.props.products.favorite}
                            onClick={this.props.onClick}
                        />
                    </div>

                    <ModalBtnAddCard
                        isOpen = {this.state.showModal}
                        zone={()=>{
                            this.btnClickHandler()
                            this.setScroll()
                        }}
                        modalClose={()=>{
                            this.btnClickHandler()
                            this.setScroll()
                        }}
                    />
                </div>
            </div>
        )
    }
}

export default ProductBlock