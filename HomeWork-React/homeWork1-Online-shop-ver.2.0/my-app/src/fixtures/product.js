const getProduct = ()=> new Promise(function (resolve){
resolve([
    {
        name:"Телевизор Samsung QE55Q60AAUXUA",
        price: "28 999",
        img:"https://content2.rozetka.com.ua/goods/images/big/225787301.jpg",
        id:"297615813",
        color:"red",
    },

    {
        name:"Телевизор Samsung UE43T5300AUXUA",
        price: "11 699",
        img:"https://content2.rozetka.com.ua/goods/images/big/174794113.jpg",
        id:"206672437",
        color:"black",
    },
    {
        name:"Телевизор Samsung UE32T4500AUXUA",
        price: "8 399",
        img:"https://content2.rozetka.com.ua/goods/images/big/174793941.jpg",
        id:"200441875",
        color:"black",
    },
    {
        name:"Телевизор Samsung UE50AU7100UXUA",
        price: "16 999",
        img:"https://content2.rozetka.com.ua/goods/images/big/178627142.jpg",
        id:"297597283",
        color:"black",
    },
    {
        name:"Телевизор Samsung QE65Q70AAUXUA",
        price: "47 999",
        img:"https://content2.rozetka.com.ua/goods/images/big/225787301.jpg",
        id:"297618733",
        color:"red",
    },
    {
        name:"Телевизор Samsung QE43Q60TAUXUA",
        price: "17 999",
        img:"https://content1.rozetka.com.ua/goods/images/big/194224776.jpg",
        id:"200297473",
        color:"red",
    },
    {
        name:"Телевизор Samsung UE55TU8500UXUA",
        price: "19 499",
        img:"https://content1.rozetka.com.ua/goods/images/big/194224101.jpg",
        id:"200240287",
        color:"black",
    },
    {
        name:"Телевизор Samsung QE50QN90AAUXUA",
        price: "39 999",
        img:"https://content2.rozetka.com.ua/goods/images/big/225787301.jpg",
        id:"291592908",
        color:"red",
    },
    {
        name:"Телевизор Samsung QE55QN85AAUXUA",
        price: "46 999",
        img:"https://content2.rozetka.com.ua/goods/images/big/225787301.jpg",
        id:"291564038",
        color:"red",
    },
    {
        name:"Телевизор Samsung UE32T5300AUXUA",
        price: "9 599",
        img:"https://content.rozetka.com.ua/goods/images/big/174793070.jpg",
        id:"200449747",
        color:"black",
    },
    {
        name:"Телевизор Samsung UE55AU9000UXUA",
        price: "22 499",
        img:"https://content.rozetka.com.ua/goods/images/big/225783221.jpg",
        id:"297615223",
        color:"red",
    }

])
})
export default getProduct;