import React from "react";
import getProduct from "./fixtures/product";

class App extends React.Component{
  constructor(props) {
    super(props);

  }
  componentDidMount() {
    getProduct()
        .then((data) => {
          this.setState({ item: data });
        })
  }

  render() {
    return (
        <>

            <div className="header">
                <img src="https://cdn.icon-icons.com/icons2/1580/PNG/128/2849824-basket-buy-market-multimedia-shop-shopping-store_107977.png" alt="" className="header__logo"/>
            </div>


            {this.props.products.map((item) => {
                return(
                    <div className="product-card" id={this.props.item.id}>
                        <div className="product-card__content">
                            <img src={this.props.item.img} alt="" className="product-card__img"/>
                            <h2 className="product-card__name" >{this.props.item.name}</h2>
                            <p typeof="number" className="product-card__price" style={{color:this.props.item.color}}>{this.props.item.price}₴</p>

                            {this.props.onClick && <svg fill="black" className={`product-card__img-star ${this.props.item.isActive ? "active" : "notActive"}`} onClick={this.props.item.onClick} id={this.props.item.id} role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                <path id={this.props.item.id} d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                            </svg>}

                            <button className="product-card__add-card">Add to cart  </button>
                        </div>
                    </div>
                )
            })}

        </>
    );
  }
}
export default App;
