
// написать функцию реверса строки ( пользуются на собеседованиях)
//revers('String') === 'gn1rts';
/*
const revers = (str) =>{
    let reversString = "";

    //for (let letter of str){                      // или  так
   //     reversString = letter + reversString;
    //}

    for (let i = str.length - 1; i >=0 ; i--){     // или так
        console.log(str[i]);
        reversString = reversString + str[i];
    }

    return reversString
};
console.log(revers("Strings"));
 */

// палиндром(читается в обе стороны одинаково)
// написать функцию которая позволит вернуть значение true ,
// если строка является полиндроменой , и false - усли нет
// при этом нужно учитывать пробелы и знаки припинания
/*
let palindrom = (str) => {
    str = str.trim(); // удаляем пробелы с переди и зади
    let reversString = "";      // переменная которая будет хранить строку

    for (let i = str.length - 1; i >=0 ; i--){     // или так
        reversString = reversString + str[i];
    }

    return reversString.toLowerCase() === str.toLowerCase(); //этим мы делаем приведение к false true к строгому сравнению
};

console.log(palindrom('Hello'));
console.log(palindrom('Radar'));
 */


//анаграма (это слово букв с которых можно сложить эти слова)
//написать функцию которая проверяет являются две строки анаграмными
//учитывая только символы
// finder === friend
/*
const anagram = (strA , strB) =>{

      if (strA.length !== strB.length){
          return false;
      }

      for (let letter of strA){
          let index = strB.indexOf(letter);

          if (index === -1){   // перебор букв если ее нету то -1
              return false;
          }
      strB = strB.slice(0,index) + strB.slice(index + 1, strB.length );

      }
      return true;
};

console.log(anagram("наказ" , "казан"));
console.log(anagram("питер" , "курить"));
 */

// реализовать очередь (например электронная)  !! структуры данных ,функция которая будет ложить в себя данные
/*
const queue = {
    start: 1,   // начало очереди
    finish: 0,    // конец очереди
    get: () => {
        let key = "objName" + queue.start;
            const value = queue[key];
            delete queue[key];
            queue.start++;

            return value;
    },
    set: (value) => {
        queue.finish++;
        let key = "objName" + queue.finish;
        this[key] = value;
    },
};

queue.set('Ivan');
console.log(queue);
queue.set("Valerii");
console.log(queue);
console.log(queue.get());
console.log(queue.get());
queue.set("Mark");
console.log(queue.get());
 */


// написать функцию которая поверхностно находит пересечение обьектов ( в каких обьектах будет одинаковые значения)
// и возвращает обьект переччений
/*
const intersection = (objA,objB) => {
    let resultObj = {};
    let objKeys = '';

    for (const key in objB){
        objKeys = `${objKeys} ${key}`;
    }

    for (const key in objA){
        const hasKey = objKeys.indexOf(key)!== -1;

        if (hasKey){
            resultObj[key] = true;
        }
    }
    return resultObj;
};
console.log(intersection(
    {a:undefined,b:23,c:false},
    {a:undefined,c: false,d:"string"}
    )
);
 */


// написать функц которая возваращает новый обьект
// без указаных значений





