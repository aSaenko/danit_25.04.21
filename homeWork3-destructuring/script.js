// Задание 1

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
//
// const compound = [...clients1 , ...clients2];
// console.log(compound);


// Задание 2

// const characters = [
//     {
//         name: "Елена",
//         lastName: "Гилберт",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Кэролайн",
//         lastName: "Форбс",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Аларик",
//         lastName: "Зальцман",
//         age: 31,
//         gender: "man",
//         status: "human"
//     },
//     {
//         name: "Дэймон",
//         lastName: "Сальваторе",
//         age: 156,
//         gender: "man",
//         status: "vampire"
//     },
//     {
//         name: "Ребекка",
//         lastName: "Майклсон",
//         age: 1089,
//         gender: "woman",
//         status: "vempire"
//     },
//     {
//         name: "Клаус",
//         lastName: "Майклсон",
//         age: 1093,
//         gender: "man",
//         status: "vampire"
//     }
// ];
//
// for (let {name: firstName, lastName: secondName, age} of characters) {
//     let charactersShortInfo = (firstName + " " + secondName + " " + age);
//     console.log(charactersShortInfo)
// }


// Задание 3

// const user1 = {
//     name: "John",
//     years: 30
// };
//
// let {name:name,years:age , isAdmin} = user1;
// console.log(name);
// console.log(age);
// console.log(isAdmin ? undefined = true:false);


// Задание 4

// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//         lat: 38.869422,
//         lng: 139.876632
//     }
// }
//
// const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
// }
//
// const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto',
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
// }
//
// let fullProfile = {...satoshi2018, ...satoshi2019,...satoshi2020};
// console.log(fullProfile);


// Задание 5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
// }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
// }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
// }];
//
// const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
// }
//
// let newBook = {name , author} = bookToAdd;
// let test = books.push(newBook);
// console.log(books)

// Задание 6
// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
// }
//
// let obj = {name,surname} = employee;
// obj.age = 12;
// obj.salary = 133;
//
// console.log(employee);
// console.log(obj);


// Задание 7
const array = ['value', () => 'showValue'];

let [value ,showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
