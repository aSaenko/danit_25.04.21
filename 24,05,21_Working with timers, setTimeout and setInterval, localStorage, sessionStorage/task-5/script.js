/**
 * Змінювати зображення слайду
 * кожні 2 секунди
 *
 * ADVANCED: розкоментувати кнопки в html
 * - можна включати або виключати режим повторення слайдів
 * - слайди можуть змінюватися кожні 2 секунди або
 * за допмогою натискань Prev / Next кнопок
 *
 * Для виконання ADVANCED потрібно розібратися з таймерами та їх очищенням
 * clearTimeout
 *
 */

const slides = [
  "https://images.freeimages.com/images/large-previews/bf4/the-road-through-the-woods-1449194.jpg",
  "https://images.freeimages.com/images/large-previews/e12/corn-field-1-1368931.jpg",
  "https://images.freeimages.com/images/large-previews/468/winter-wonderland-1383617.jpg",
  "https://images.freeimages.com/images/large-previews/9b6/among-giants-1375605.jpg",
];
