/**
 * Зробити лічильник, який буде збільшуватися
 * на 1 щосекунди
 *
 * ADVANCED:
 * - Дати змогу зупиняти / запускати / скидувати лічильник
 * (розкоментувати кнопки в html)
 * - Якщо лічильник запущений, то робити кнопки Start та
 * Reset не активними (властивість disabled)
 * - Якщо лічильник зупинений, то тільки кнопка Pause
 * повинна бути не активною
 * Потрібно використати setInterval та clearInterval
 *
 */
let timerId;
let i = 0;
let elTimer = document.getElementById("counter");
let elStartBtn = document.getElementById("start");
let elStoptBtn = document.getElementById("stop");
let elResetBtn = document.getElementById("reset");

function timerUpdate(counter = 0) {
  i = counter;
  elTimer.innerText = counter;
}

function clock() {
  timerUpdate(++i);
}

function startClickHandler() {
  if (!!timerId) return;

  timerId = setInterval(clock, 1000, i);
}
elStartBtn.addEventListener("click", startClickHandler);

function stopClickHandler() {
  clearInterval(timerId);
  timerId = null;
}
elStoptBtn.addEventListener("click", stopClickHandler);

function resetClickHandler() {
  stopClickHandler();
  timerUpdate();
}
elResetBtn.addEventListener("click", resetClickHandler);
