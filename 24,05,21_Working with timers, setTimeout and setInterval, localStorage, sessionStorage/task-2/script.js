/**
 * Створити годинник
 *
 */

setInterval(updateClock, 1000);

function setClockView(day, hours, minutes, seconds) {
  let elDay = document.querySelector(".clock-day");
  let elHours = document.querySelector(".clock-hours");
  let elMinutes = document.querySelector(".clock-minutes");
  let elSeconds = document.querySelector(".clock-seconds");

  elDay.innerText = day;
  elHours.innerText = hours;
  elMinutes.innerText = minutes;
  elSeconds.innerText = seconds;
}

function updateClock() {
  let dateNow = new Date();
  let hours = dateNow.getHours();
  let minutes = dateNow.getMinutes();
  let seconds = dateNow.getSeconds();
  let day = dateNow.getDay();

  switch (day) {
    case 0:
      day = "SU";
      break;
    case 1:
      day = "MO";
      break;
    case 2:
      day = "TU";
      break;
    case 3:
      day = "WE";
      break;
    case 4:
      day = "TH";
      break;
    case 5:
      day = "FR";
      break;
    case 6:
      day = "SA";
      break;
  }

  setClockView(day, hours, minutes, seconds);
}
