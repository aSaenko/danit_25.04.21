import Photo from "./Photo.js";

class Album {
  photos = [];

  constructor(containerId) {
    this.container = document.getElementById(containerId);
  }

  updatePhotos(photos) {
    if (!Array.isArray(photos)) return;

    this.photos = photos;

    this.render();
  }

  render() {
    this.container.innerHTML = "";
    this.container.insertAdjacentHTML(
      "afterbegin",
      `<div class="gallery">${this.getTemplate()}</div>`
    );
  }


  getTemplate(){   // шаблоны
    const photo = new Photo()
    return this.photos.reduce((acc, {author , download_url }) => {
      const photoProps = {src: download_url , link:download_url,author }
      photo.updatePhoto(photoProps);
      return(acc += photo.getTemplate());
    },"");
  }
}

export default Album;


