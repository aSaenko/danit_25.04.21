class Pagiation {
  currentPage = 1;
 // переключалка
  constructor(containerId, prevText = "prev", nextText = "next") {
    this.container = document.getElementById(containerId);
    this.nextText = nextText;
    this.prevText = prevText;
  }

  render() {
    const template = `<div class="pagination">
        <button class="paggination_btn" id='pagination-next'>${this.prevText}</buttton>
        <span class="pagination__index">${this.currentPage}</span>
        <button class="paggination_btn" id='pagination-next'>${this.nextText}</buttton>
    </div>`;

    this.container.insertAdjacentHTML("beforeend", template);
  }

  attachEvent(getPage) {
    const nextBtnElement = this.container.getElementById("pagination-next");
    const prevButtonElement = this.container.getElementById("pagination-prev");

    prevButtonElement.addEventListener("click", () => {
      this.currentPage = this.currentPage - 1 < 0 ? 0 : --this.currentPage;
      getPage(this.currentPage);
    });
    nextBtnElement.addEventListener("click", () => {
      this.currentPage++;
      getPage(this.currentPage);
    });
  }
}

export default Pagiation;
