/*
 * Имплементировать компонент Фотографии
 *
 * необходимо отображать информацию о фотографии
 * Advanced: необходимо отображать информацию о фотографии в модальном окне
 */
//все что здесь написано эо пример модуля album
class Photo { // что принимаем
  constructor(src, author,link,containerId) {
    this.src = src;
    this.author = author;
    this.link = link;
    this.container = document.getElementById(containerId);
  }

  showInformation() {}

  render() {  // что выводим

    this.container.innerHTML = ""
    this.container.insertAdjacentHTML("afterbegin" , this.getTemplate());
  }

  updatePhoto({src, author,link,}){
    this.src = src;
    this.author = author;
    this.link = link;
  }
  getTemplate(){
    return `<div className="card" style="width: 18rem;">
      <div className="card-body">
        <h5 className="card-title">Card title</h5>
        <h6 className="card-subtitle mb-2 text-muted">Card subtitle</h6>
        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's
          content.</p>
        <a href="#" className="card-link">Card link</a>
        <a href="#" className="card-link">Another link</a>
      </div>
    </div>`
  }
}

export default Photo