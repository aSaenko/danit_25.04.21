// App entry point (изначальная точка) с этим файлом все остальные модули содействуют

import * as photoService from "./services/photo.js";
import renderImage from "./utils/renderImage.js";
import Album from "./components/Album";
import Pagination from "./components/Pagination";

const album = new Album("gallery")
const pagination = new Pagination("pagination");

album.render();
pagination.render();

photoService.getPhotos().then((list) => {
  album.updatePhotos(list);
  album.render()
  pagination.render();
});

