export default function renderImage(container, image) {
  let { download_url: url } = image;
  if (container.querySelector("img")) {
    container.querySelector("img").remove();
  }
  const elImage = document.createElement("img");
  elImage.src = url;
  container.append(elImage);
}
