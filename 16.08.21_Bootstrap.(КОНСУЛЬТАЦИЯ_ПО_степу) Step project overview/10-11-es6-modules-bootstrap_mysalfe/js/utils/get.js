function get(url, queryParams) {
  let queryParamsString = new URLSearchParams(queryParams);
  return fetch(`${url}?${queryParamsString}`).then((rsp) => {
    if (rsp.ok) {
      return rsp.json();
    } else {
      throw new Error(rsp.status);
    }
  });
}
export default get;
