import get from "../utils/get.js";

export const BASE_URL = "https://picsum.photos/v2";
export const PHOTOLIMITS = 9;

export async function getPhotos(page = 1) {
  try {
    return await get(BASE_URL + "/list", { page, limit: PHOTOLIMITS });
  } catch (e) {
    throw new Error(e);
  }
}

export async function hasPhotoNextPage(curentPage) {
  const photos = await get(BASE_URL + "/list", {
    page: curentPage + 1,
    limit: PHOTOLIMITS,
  });
  return Array.isArray(photos) && photos.length > 0;
}

export default getPhotos;
