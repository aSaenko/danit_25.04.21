class Album {
  photos = [];

  constructor(containerId) {
    this.container = document.getElementById(containerId);
  }

  updatePhotos(photos) {
    if (!Array.isArray(photos)) return;

    this.photos = photos;
    this.container.innerHTML = "";
    this.render();
  }

  render() {
    const template = this.photos.reduce((acc, photo) => {
      return (acc += `<div class="photo">
        <div class="photo__description">
            <img src="${photo.download_url}" />
          <h3>${photo.title}</h3>
          <a class="photo__download" href="${photo.download_url}" target="_blank">Download</a>
        </div>
      </div>`);
    }, "");

    this.container.insertAdjacentHTML(
      "afterbegin",
      `<div class="gallery">${template}</div>`
    );
  }
}

export default Album;
