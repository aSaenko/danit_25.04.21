// App entry point

import * as photoService from "./services/photo.js";
import renderImage from "./utils/renderImage.js";

photoService.getPhotos().then((list) => {
  const elPhotosList = document.querySelectorAll(".photo");
  for (let i = 0; i < list.length; i++) {
    renderImage(elPhotosList[i], list[i]);
  }
});
